#include "hjarta/core/Application.h"
#include "hjarta/input/InputManager.h"
#include "hjarta/tools/Log.h"
#include "hjarta/tools/Profiler.h"

int main(int argc, char *argv[])
{
    hjarta::InstanceProvider<hjarta::Application>::create();
    HJARTA_APPLICATION().init(hjarta::Size2D<int>(1600, 900), "Engine", 60);

    HJARTA_PROFILER().init("profiler");
    HJARTA_PROFILER().startScope("Init", hjarta::colors::random());

    while (!HJARTA_INPUT_MANAGER().hasQuitRequest())
    {
        HJARTA_INPUT_MANAGER().update();

        HJARTA_RENDERER().clear(hjarta::colors::MsvLightGrey);
        HJARTA_RENDERER().present();
        HJARTA_APPLICATION().eol(hjarta::Application::LimitFPS::No);
    }

    HJARTA_APPLICATION().terminate();

    exit(EXIT_SUCCESS);
}