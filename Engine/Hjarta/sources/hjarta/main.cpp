#include "hjarta/core/Application.h"
#include "hjarta/input/InputManager.h"

int main(int argc, char *argv[])
{
    hjarta::InstanceProvider<hjarta::Application>::create();
    HJARTA_APPLICATION().init(hjarta::Size2D<int>(1200, 800), "Vikingr! - Hjarta", 60);


    while (!HJARTA_INPUT_MANAGER().hasQuitRequest())
    {
        HJARTA_INPUT_MANAGER().update();
        
        HJARTA_RENDERER().clear();
        HJARTA_RENDERER().present();
        HJARTA_APPLICATION().eol(hjarta::Application::LimitFPS::No);
    }

    HJARTA_APPLICATION().terminate();

    exit(EXIT_SUCCESS);
}