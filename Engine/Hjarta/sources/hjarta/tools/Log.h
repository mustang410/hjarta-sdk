#ifndef _SCX_LOG_H_
#define _SCX_LOG_H_

#include "hjarta/core/InstanceProvider.h"
#include "hjarta/core/Types.h"

#include <mutex>
#include <string>
#include <sstream>
#include <fstream>

#define HJARTA_USE_ASSERTIONS
#ifdef HJARTA_USE_ASSERTIONS
#define HJARTA_DEBUG_BREAK() __debugbreak()
#define HJARTA_ASSERT(expr, ...) if (!(expr)) {         \
    HJARTA_LOG_ERROR("Assert on: (", #expr, ")");       \
    HJARTA_LOG_ERROR(__VA_ARGS__);                      \
    HJARTA_DEBUG_BREAK();                               \
}

#define HJARTA_VERIFY(expr, ...) [=]() {                \
    if (expr) return true;                              \
    else                                                \
    {                                                   \
        HJARTA_ASSERT(expr, __VA_ARGS__);               \
        return false;                                   \
    }                                                   \
}()                                                     \

#define HJARTA_ASSERT_FCT(expr, ...) if (!(expr)) {     \
    HJARTA_LOG_FCT_ERROR("Assert on: (", #expr, ")");   \
    HJARTA_LOG_FCT_ERROR(__VA_ARGS__);                  \
    HJARTA_DEBUG_BREAK(); \
}

#define HJARTA_VERIFY_FCT(expr, ...) [=]() {            \
    if (expr) return true;                              \
    else                                                \
    {                                                   \
        HJARTA_ASSERT_FCT(expr, __VA_ARGS__);           \
        return false;                                   \
    }                                                   \
}()                                                     \

#define HJARTA_ASSERT_UNIMPL() {\
    HJARTA_LOG_ERROR("Method ", __FUNCTION__, " in unimplemented!"); \
    HJARTA_DEBUG_BREAK(); \
}
#else
#define HJARTA_DEBUG_BREAK()
#define HJARTA_ASSERT(expr, ...)
#define HJARTA_ASSERT_FCT(expr, ...)
#define HJARTA_ASSERT_UNIMPL() { \
    HJARTA_LOG_ERROR("Method ", __FUNCTION__, " in unimplemented!"); \
}
#endif // HJARTA_USE_ASSERTIONS

#define HJARTA_LOG()                    hjarta::InstanceProvider<hjarta::Log>::get()
#define HJARTA_LOG_INFO(...)            HJARTA_LOG().print(hjarta::Log::MessageType::Info, typeid(*this).name(), __VA_ARGS__)
#define HJARTA_LOG_DEBUG(...)           HJARTA_LOG().print(hjarta::Log::MessageType::Debug, typeid(*this).name(), __VA_ARGS__)
#define HJARTA_LOG_WARNING(...)         HJARTA_LOG().print(hjarta::Log::MessageType::Warning, typeid(*this).name(), __VA_ARGS__)
#define HJARTA_LOG_ERROR(...)           HJARTA_LOG().print(hjarta::Log::MessageType::Error, typeid(*this).name(), __VA_ARGS__)
#define HJARTA_LOG_FCT_INFO(...)        HJARTA_LOG().print(hjarta::Log::MessageType::Info, __FUNCTION__, __VA_ARGS__)
#define HJARTA_LOG_FCT_DEBUG(...)       HJARTA_LOG().print(hjarta::Log::MessageType::Debug, __FUNCTION__, __VA_ARGS__)
#define HJARTA_LOG_FCT_WARNING(...)     HJARTA_LOG().print(hjarta::Log::MessageType::Warning, __FUNCTION__, __VA_ARGS__)
#define HJARTA_LOG_FCT_ERROR(...)       HJARTA_LOG().print(hjarta::Log::MessageType::Error, __FUNCTION__, __VA_ARGS__)

namespace hjarta
{
    // required to authorized singleton operation on input handler by the application
    class Application;

    // ## Log
    // Provides basic tools to handle logs
    class Log
    {
    public:
        // path of the logger output 
        static const std::string OutputPath;

        // ### MessageType
        // Different kinds of log levels
        enum class MessageType { Info, Debug, Warning, Error };


    public:
        Log();
        ~Log();

    public:
        /// ##### Class interface
        void print(const MessageType &_type, const std::string &_module, const std::string &_message);
        template <class T, class... Args>
        void print(const MessageType &_type, const std::string &_module, const std::string &_message, const T &_obj, Args... _args);

    private:
        /// ##### Internal methods
        void print_async(const MessageType &_type, const std::string &_module, const std::string &_message);

    private:
        /// ##### Attributes
        std::ofstream   m_output;
        std::mutex      m_outputMutex;
    };

    //------------------------------------------------------------------------------------------

    template <class T, class... Args>
    void Log::print(const MessageType &_type, const std::string &_module, const std::string &_message, const T &_obj, Args... _args)
    {
        std::stringstream buffer;
        buffer << _message << _obj;
        print(_type, _module, buffer.str(), _args...);
    }
}


#endif // _SCX_LOG_H_