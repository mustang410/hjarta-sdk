#ifndef HJARTA_Profiler_h
#define HJARTA_Profiler_h

#include "hjarta/core/Types.h"
#include "hjarta/serialization/Serializer.h"
#include "hjarta/event/EventDispatcher.h"
#include "hjarta/input/InputEvent.h"
#include "hjarta/gfx/Color.h"

#define HJARTA_PROFILER() hjarta::InstanceProvider<hjarta::Profiler>::get()
#define HJARTA_PROFILER_SCOPE(id, color) hjarta::Profiler::AutoScope autoScope(id, color)

namespace hjarta
{
    // ## Profiler
    // Profile execution times
    // Use HJARTA_PROFILER_SCOPE to automatically profile a scope using an id and a color
    class Profiler : public IEventListener
    {
    public:
        /// ##### Custom types
        enum class Recording { Yes, No };

        // ## Scope
        // Represent a timed scope
        class Scope : public Serializable
        {
            HJARTA_DECL_RTTI(hjarta::Profiler::Scope, hjarta::Serializable)

        public:
            /// ##### Serialization
            void                        serialize(Serializer& _serializer) override;

        public:
            Scope() = default;
            Scope(const std::string& _id, const Color& _color); // root constructor
            Scope(Scope* _parent, const std::string& _id, const Color& _color, unsigned _frame);
            ~Scope();

        public:
            /// ##### Scope management
            void                        start();
            void                        stop();
            Scope*                      addScope(const std::string& _id, const Color& _color, unsigned _frame);
            void                        clear();

        public:
            /// ##### ProfiledScope interface
            Scope*                      getParent() { return m_parent; }
            const std::string&          getId() const { return m_id; }
            const Color&                getColor() const { return m_color; }
            Millisecond                 getStart() const { return m_start; }
            bool                        isStarted() const { return m_start > 0; }
            Millisecond                 getEnd() const { return m_end; }
            bool                        isStopped() const { return m_end > 0; }
            Millisecond                 getDuration() const { return m_duration; }
            const std::vector<Scope*>&  getScopes() const { return m_scopes; }
        private:
            /// ##### Attributes
            std::string                 m_id;
            unsigned                    m_frame = 0;
            Color                       m_color;
            Millisecond                 m_start = 0;
            Millisecond                 m_end = 0;
            Millisecond                 m_duration = 0;
            std::vector<Scope*>         m_scopes;
            Scope*                      m_parent = nullptr;
        };

        // ## AutoScope
        // Performs the start & stop automatically
        class AutoScope
        {
        public:
            AutoScope() = default;
            AutoScope(const std::string& _id, const Color& _color);
            ~AutoScope();

        private:
            std::string                 m_id;
        };

    public:
        Profiler();

    public:
        /// ##### Profiler interface
        // Profiler has to be initialized to start / stop recording
        // Once initialized, Profiler can be started / stopped by pressing the specified key
        // Scopes are dumped every _fmax frame or when stopping the recording
        void                            init(const std::string& _directory, unsigned _fmax = 1000, ScanCode _key = SDL_SCANCODE_F10);
        // Explicitly start / stop recording can be done without using toggle key
        void                            startRecording();
        void                            stopRecording();
        bool                            isRecording() const { return m_recording == Recording::Yes; }
        void                            eol();
        /// ##### Scope interface
        void                            startScope(const std::string& _id, const Color& _color);
        void                            stopScope(const std::string& _id);

    private:
        /// ##### Internals
        void                            processKey(InputEventKey& _event);
        std::string                     buildDumpName();
        void                            dump();

    private:
        /// ##### Attribute
        Recording                       m_recording = Recording::No;
        // recording options
        ScanCode                        m_recordKey;
        std::string                     m_directory;
        std::string                     m_dumpBaseName;
        unsigned                        m_fmax;
        // current state
        unsigned                        m_fcounter = 0;
        Scope                           m_root;
        Scope*                          m_current = nullptr;

    };
}

#endif // HJARTA_Profiler_h