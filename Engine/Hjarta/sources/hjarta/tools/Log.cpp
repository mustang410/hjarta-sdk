#include "hjarta/tools/Log.h"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace hjarta
{
    // path of the logger output
    const std::string Log::OutputPath = "log.txt";

    //------------------------------------------------------------------------------------------

    Log::Log() : m_output(OutputPath)
    {
        print_async(MessageType::Info, __FUNCTION__, "Log created");
    }

    //------------------------------------------------------------------------------------------

    Log::~Log()
    {
        print_async(MessageType::Info, __FUNCTION__, "Log closed");
        m_output.close();
    }

    //------------------------------------------------------------------------------------------

    void Log::print(const MessageType &_type, const std::string &_module, const std::string &_message)
    {
        InstanceProvider<Log>::get().print_async(_type, _module, _message);
    }

    //------------------------------------------------------------------------------------------

    void Log::print_async(const MessageType &_type, const std::string &_module, const std::string &_message)
    {
        std::lock_guard<std::mutex> guard(m_outputMutex);

        std::stringstream buffer;
        time_t t = time(0); // get time now
        tm now;
        buffer << std::setfill('0');
        if (localtime_s(&now, &t) == 0) buffer << "[" << std::setw(2) << now.tm_hour << ":" << std::setw(2) << now.tm_min << ":" << std::setw(2) << now.tm_sec << "] ";
        else buffer << "[invalid time] ";

        // formatting
        switch (_type)
        {
        case MessageType::Info:
            buffer << "[Info] ";
            break;

        case MessageType::Debug:
            buffer << "[Debug] ";
            break;

        case MessageType::Warning:
            buffer << "[Warning] ";
            break;

        case MessageType::Error:
            buffer << "[Error] ";
            break;
        }

        buffer << "[" << _module << "] ";
        buffer << _message << std::endl;

        m_output << buffer.str();
        std::cout << buffer.str();
    }
}