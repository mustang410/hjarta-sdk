#include "hjarta/tools/Profiler.h"
#include "hjarta/input/InputManager.h"

#include <iomanip>
#include <time.h>

namespace hjarta
{
    void Profiler::Scope::serialize(Serializer& _serializer)
    {
        _serializer.serializeObject("id", &m_id);
        _serializer.serializeObject("frame", &m_frame);
        _serializer.serializeObject("color", &m_color);
        _serializer.serializeObject("start", &m_start);
        _serializer.serializeObject("end", &m_end);
        _serializer.serializeObject("duration", &m_duration);
        _serializer.serializeContainer("scopes", &m_scopes, Serializer::Mandatory, &HJARTA_RTTI_FACTORY());
    }

    //------------------------------------------------------------------------------------------

    Profiler::Scope::Scope(const std::string& _id, const Color& _color) : Scope(nullptr, _id, _color, 0) {}

    //------------------------------------------------------------------------------------------

    Profiler::Scope::Scope(Scope* _parent, const std::string& _id, const Color& _color, unsigned _frame)
    {
        m_parent = _parent;
        m_id = _id;
        m_frame = _frame;
        m_color = _color;
    }

    //------------------------------------------------------------------------------------------

    Profiler::Scope::~Scope()
    {
        clear();
    }

    //------------------------------------------------------------------------------------------

    void Profiler::Scope::start()
    {
        m_start = SDL_GetTicks();
        m_end = 0;
    }

    //------------------------------------------------------------------------------------------

    void Profiler::Scope::stop()
    {
        HJARTA_ASSERT(m_start > 0, "ProfiledScope is not started");
        HJARTA_ASSERT(m_scopes.empty() || m_scopes.back()->isStopped(), "Child scope is not stopped");
        m_end = SDL_GetTicks();
        m_duration = m_end - m_start;
    }

    //------------------------------------------------------------------------------------------

    void Profiler::Scope::clear()
    {
        for (Scope* scope : m_scopes)
        {
            delete scope;
        }
        m_scopes.clear();
    }

    //------------------------------------------------------------------------------------------

    Profiler::Scope* Profiler::Scope::addScope(const std::string& _id, const Color& _color, unsigned _frame)
    {
        HJARTA_ASSERT(m_scopes.empty() || m_scopes.back()->isStopped(), "Previous child scope is not stopped");
        Scope* scope = new Scope(this, _id, _color, _frame);
        scope->start();
        m_scopes.emplace_back(scope);
        return scope;
    }

    //------------------------------------------------------------------------------------------

    Profiler::AutoScope::AutoScope(const std::string& _id, const Color& _color)
    {
        m_id = _id;
        HJARTA_PROFILER().startScope(m_id, _color);
    }

    //------------------------------------------------------------------------------------------

    Profiler::AutoScope::~AutoScope()
    {
        if (!m_id.empty())
        {
            HJARTA_PROFILER().stopScope(m_id);
        }
    }

    //------------------------------------------------------------------------------------------

    Profiler::Profiler() : m_root("Root", hjarta::colors::MsvPurple)
    {
        m_root.start();
        m_current = &m_root;

        HJARTA_RTTI_FACTORY().registerClass<Scope>();
    }

    //------------------------------------------------------------------------------------------

    void Profiler::init(const std::string& _directory, unsigned _fmax, ScanCode _key)
    {
        m_directory = _directory;
        m_fmax = _fmax;
        m_recordKey = _key;
        HJARTA_INPUT_MANAGER().registerEvent<InputEventKey>(this, &Profiler::processKey);
    }

    //------------------------------------------------------------------------------------------

    void Profiler::startRecording()
    {
        if (HJARTA_VERIFY(!m_directory.empty(), "Profiler is not initialized"))
        {
            struct tm date;
            time_t now = time(0);
            localtime_s(&date, &now);

            std::stringstream stream;
            stream << "dump_";
            stream << std::setfill('0') << std::setw(2) << (date.tm_mon + 1) << std::setfill('0') << std::setw(2) << date.tm_mday << "_";
            stream << std::setfill('0') << std::setw(2) << date.tm_hour << std::setfill('0') << std::setw(2) << date.tm_min << "_";
            m_dumpBaseName = stream.str();

            HJARTA_LOG_INFO("Profiler started");
            m_recording = Recording::Yes;
        }
    }

    //------------------------------------------------------------------------------------------

    void Profiler::stopRecording()
    {
        while (m_current != &m_root)
        {
            m_current->stop();
            m_current = m_current->getParent();
        }

        dump();

        HJARTA_LOG_INFO("Profiler stopped");
        m_recording = Recording::No;
    }

    //------------------------------------------------------------------------------------------

    void Profiler::eol()
    {
        if (isRecording())
        {
            m_fcounter++;
            if (m_fcounter % m_fmax == 0)
            {
                dump();
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void Profiler::startScope(const std::string& _id, const Color& _color)
    {
        if (m_recording == Recording::Yes)
        {
            m_current = m_current->addScope(_id, _color, m_fcounter);
        }
    }

    //------------------------------------------------------------------------------------------

    void Profiler::stopScope(const std::string& _id)
    {
        if (m_recording == Recording::Yes)
        {
            if (m_current && m_current->getId() == _id)
            {
                m_current->stop();
                m_current = m_current->getParent();
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void Profiler::processKey(InputEventKey& _event)
    {
        if (!_event.isConsumed() && _event.getScancode() == m_recordKey && _event.getType() == InputEventKey::KeyUp)
        {
            if (m_recording == Recording::No)
            {
                startRecording();
            }
            else
            {
                stopRecording();
            }

            _event.consume();
        }
    }

    //------------------------------------------------------------------------------------------

    std::string Profiler::buildDumpName()
    {
        return m_directory + "/" + m_dumpBaseName + std::to_string(m_fcounter) + ".json";
    }

    //------------------------------------------------------------------------------------------

    void Profiler::dump()
    {
        m_root.saveToFileURL(buildDumpName());
        m_root.clear();
        HJARTA_LOG_INFO("Profiler dumped");
    }
}