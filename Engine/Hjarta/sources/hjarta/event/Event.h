#ifndef HJARTA_Event_h
#define HJARTA_Event_h

#include "hjarta/core/RTTI.h"

#define HJARTA_DECL_EVENT(EventType, Parent)        \
        HJARTA_DECL_RTTI(EventType, Parent)         \

namespace hjarta
{
    // ## Event
    // Base class for events
    class Event : public RTTI
    {
        HJARTA_DECL_RTTI(hjarta::Event, hjarta::RTTI);

    public:
        /// ##### Custom types
        enum class Local { Yes, No };
        enum class Consumed { Yes, No };
        enum class Skipped { Yes, No };

    public:
        Event(Local _local = Local::No) : m_local(_local) {}

    public:
        /// ##### Event interface
        // Local event are sent only to listener and are not dispatched to subdispatchers
        void        setLocal(Local _local) { m_local = _local; }
        bool        isLocal() const { return m_local == Local::Yes; }
        // A consumed event can still be processed by other listeners
        void        consume() { m_consumed = Consumed::Yes; }
        bool        isConsumed() const { return m_consumed == Consumed::Yes; }
        // Skipping an event will stop its forwarding to other listeners
        void        skip() { m_skipped = Skipped::Yes; }
        bool        isSkipped() const { return m_skipped == Skipped::Yes; }

    private:
        /// ##### Attributes
        Local       m_local = Local::No;
        Consumed    m_consumed = Consumed::No;
        Skipped     m_skipped = Skipped::No;
    };
}

#endif // HJARTA_Event_h