#ifndef HJARTA_EventDispatcher_inl
#define HJARTA_EventDispatcher_inl

#include "hjarta/tools/Log.h"

#include <type_traits>

namespace hjarta
{
    template <class SpecificEvent, class Listener>
    void EventDispatcher::registerEvent(Listener* _listener, void(Listener::*_callback)(SpecificEvent&))
    {
        static_assert(std::is_base_of<IEventListener, Listener>::value, "Listener has to inherith from IEventListener");
        static_assert(std::is_base_of<Event, SpecificEvent>::value, "SpecificEvent has to inherit from Event");
        CallContext_Method<SpecificEvent, Listener>* context = new CallContext_Method<SpecificEvent, Listener>(_listener, _callback);
        m_callContexts[SpecificEvent::getClassId()].emplace_back(context);
        m_register[_listener].emplace(SpecificEvent::getClassId());

        _listener->addRegisteredDispatcher(this);
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent, class Listener>
    void EventDispatcher::registerEvent(Listener* _listener, const OnEvent<SpecificEvent>& _callback)
    {
        static_assert(std::is_base_of<IEventListener, Listener>::value, "Listener has to inherith from IEventListener");
        static_assert(std::is_base_of<Event, SpecificEvent>::value, "SpecificEvent has to inherit from Event");
        CallContext_Lambda<SpecificEvent, Listener>* context = new CallContext_Lambda<SpecificEvent, Listener>(_listener, _callback);
        m_callContexts[SpecificEvent::getClassId()].emplace_back(context);
        m_register[_listener].emplace(SpecificEvent::getClassId());

        _listener->addRegisteredDispatcher(this);
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent>
    void EventDispatcher::unregisterEvent(IEventListener* _listener)
    {
        static_assert(std::is_base_of<Event, SpecificEvent>::value, "SpecificEvent has to inherit from Event");
        removeContext(SpecificEvent::getClassId(), _listener);
        m_register[_listener].erase(SpecificEvent::getClassId());

        if (m_register[_listener].empty())
        {
            m_register.erase(_listener);
            _listener->removeRegisteredDispatcher(this);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent>
    void EventDispatcher::sendEvent(SpecificEvent& _event)
    {
        if (m_active)
        {
            for (ICallContext* context : m_callContexts[SpecificEvent::getClassId()])
            {
                context->call(&_event);
                if (_event.isSkipped())
                    return;
            }
            if (!_event.isLocal())
            {
                for (auto& it : m_dispatchers)
                {
                    for (EventDispatcher* dispatcher : it.second)
                    {
                        dispatcher->sendEvent<SpecificEvent>(_event);
                        if (_event.isSkipped())
                            return;
                    }
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent, class ...Args>
    void EventDispatcher::sendEvent(Args... _args)
    {
        if (m_active)
        {
            SpecificEvent evt{ std::forward<Args>(_args)... };
            sendEvent(evt);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent, class Listener>
    void EventDispatcher::CallContext_Method<SpecificEvent, Listener>::call(Event* _event)
    { 
        SpecificEvent* specificEvent = _event->dynamicCast<SpecificEvent>();
        HJARTA_ASSERT(specificEvent != nullptr, "Event type is invalid");
        if (specificEvent)
        {
            (m_listener->*m_callback)(*specificEvent);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class SpecificEvent, class Listener>
    void EventDispatcher::CallContext_Lambda<SpecificEvent, Listener>::call(Event* _event)
    {
        SpecificEvent* specificEvent = _event->dynamicCast<SpecificEvent>();
        HJARTA_ASSERT(specificEvent != nullptr, "Event type is invalid");
        if (specificEvent && m_callback)
        {
            m_callback(specificEvent);
        }
    }
}

#endif // HJARTA_EventDispatcher_inl