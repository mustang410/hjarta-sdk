#include "hjarta/event/EventDispatcher.h"

#include <algorithm>

namespace hjarta
{
    IEventListener::~IEventListener()
    {
        std::set<EventDispatcher*> dispatchers = m_registereds;
        for (EventDispatcher* dispatcher : dispatchers)
        {
            dispatcher->unregisterAllEvents(this);
        }
    }

    //------------------------------------------------------------------------------------------

    void IEventListener::addRegisteredDispatcher(EventDispatcher* _dispatcher)
    {
        m_registereds.emplace(_dispatcher);
    }

    //------------------------------------------------------------------------------------------

    void IEventListener::removeRegisteredDispatcher(EventDispatcher* _dispatcher)
    {
        m_registereds.erase(_dispatcher);
    }

    //------------------------------------------------------------------------------------------

    void EventDispatcher::unregisterAllEvents(IEventListener* _listener)
    {
        for (RTTI::TypeId eventId : m_register[_listener])
        {
            removeContext(eventId, _listener);
        }
        m_register[_listener].clear();
        _listener->removeRegisteredDispatcher(this);
    }

    //------------------------------------------------------------------------------------------

    void EventDispatcher::registerDisptacher(EventDispatcher* _dispatcher, Priority _priority)
    {
        m_dispatchers[_priority].emplace(_dispatcher);
    }

    //------------------------------------------------------------------------------------------

    void EventDispatcher::unregisterDispatcher(EventDispatcher* _dispatcher)
    {
        for (auto& it : m_dispatchers)
        {
            it.second.erase(_dispatcher);
        }
    }

    //------------------------------------------------------------------------------------------

    void EventDispatcher::removeContext(RTTI::TypeId _eventId, IEventListener* _listener)
    {
        std::list<ICallContext*>& contexts = m_callContexts[_eventId];

        // find contexts to remove
        std::list<ICallContext*> trash;
        for (ICallContext* context : contexts)
        {
            if (context->getListener() == _listener)
            {
                trash.push_back(context);
            }
        }

        // remove them
        for (ICallContext* context : trash)
        {
            contexts.remove(context);
            delete context;
        }
    }
}