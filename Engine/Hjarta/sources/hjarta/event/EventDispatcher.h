#ifndef HJARTA_EventDispatcher_h
#define HJARTA_EventDispatcher_h

#include "hjarta/core/RTTI.h"
#include "hjarta/event/Event.h"

#include <map>
#include <list>
#include <set>
#include <functional>

#define HJARTA_EVENT_DISPATCHER() hjarta::InstanceProvider<hjarta::EventDispatcher>::get()

namespace hjarta
{
    class EventDispatcher;

    // ## IEventListener
    // Base class for event handling classes
    // Automatically unregister its callback from all registered dispatchers at destroy
    class IEventListener
    {
    public:
        virtual ~IEventListener();

    private:
        /// ##### Dispatcher friend interface
        friend class EventDispatcher;
        void                                                addRegisteredDispatcher(EventDispatcher* _dispatcher);
        void                                                removeRegisteredDispatcher(EventDispatcher* _dispatcher);

    private:
        /// ##### Attributes
        std::set<EventDispatcher*>                          m_registereds;
    };

    // ## EventDispatcher
    // Handle lists of registered IEventListener to dispatch them events
    // Dispatcher can will send any event to its registered listeners / dispatchers
    // Registered dispatchers will be notified after listeners
    class EventDispatcher : public IEventListener
    {
    public:
        /// ##### Custom types
        enum Priority : int { High = 0, Medium = 1, Low = 2 };

    public:
        /// ##### Dispatcher interface
        // Only active dispatcher will forward events to its listeners
        void                                                setEventDispatcherActive(bool _active) { m_active = _active; }
        bool                                                isEventDispatcherActive() const { return m_active; }
        // Register listeners
        template <class SpecificEvent, class Listener> void registerEvent(Listener* _listener, void(Listener::*_callback)(SpecificEvent&));
        template <class SpecificEvent> using OnEvent = std::function<void(SpecificEvent&)>;
        template <class SpecificEvent, class Listener> void registerEvent(Listener* _listener, const OnEvent<SpecificEvent>& _callback);
        template <class SpecificEvent> void                 unregisterEvent(IEventListener* _listener);
        void                                                unregisterAllEvents(IEventListener* _listener);
        // Register dispatchers
        void                                                registerDisptacher(EventDispatcher* _dispatcher, Priority _priority = Medium);
        void                                                unregisterDispatcher(EventDispatcher* _dispatcher);
        // Dispatch
        template <class SpecificEvent> void                 sendEvent(SpecificEvent& _event);
        template <class SpecificEvent, class ...Args> void  sendEvent(Args... _args);

    private:
        /// ##### Internals
        void                                                removeContext(RTTI::TypeId _eventId, IEventListener* _listener);

    private:
        /// ##### Contexts
        // Generic base without event specialization
        class ICallContext
        {
        public:
            ICallContext() = default;
            virtual IEventListener*                         getListener() const = 0;
            virtual void                                    call(Event* _event) = 0;
        };
        // CallContext for class method
        template <class SpecificEvent, class Listener>
        class CallContext_Method : public ICallContext
        {
        public:
            CallContext_Method(Listener* _listener, void(Listener::*_callback)(SpecificEvent&)) : m_listener(_listener), m_callback(_callback) {}
            virtual IEventListener*                         getListener() const override { return m_listener; }
            void                                            call(Event* _event) override;
        private:
            Listener*                                       m_listener = nullptr;
            void(Listener::*m_callback)(SpecificEvent&);
        };
        // CallContext for lambda
        template <class SpecificEvent, class Listener>
        class CallContext_Lambda : public ICallContext
        {
        public:
            CallContext_Lambda(Listener* _listener, const OnEvent<SpecificEvent>& _callback) : m_listener(_listener), m_callback(_callback) {}
            virtual IEventListener*                         getListener() const override { return m_listener; }
            void                                            call(Event* _event) override;
        private:
            Listener*                                       m_listener = nullptr;
            OnEvent<SpecificEvent>                          m_callback = nullptr;
        };

    private:
        /// ##### Attributes
        bool                                                m_active = true;
        std::map<RTTI::TypeId, std::list<ICallContext*>>    m_callContexts;
        std::map<IEventListener*, std::set<RTTI::TypeId>>   m_register;
        // dispatcher by priorities
        std::map<Priority, std::set<EventDispatcher*>>      m_dispatchers;
    };
}

#include "hjarta/event/EventDispatcher.inl"

#endif // HJARTA_EventDispatcher_h