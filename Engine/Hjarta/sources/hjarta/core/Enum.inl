#ifndef HJARTA_Enum_inl
#define HJARTA_Enum_inl

namespace hjarta
{
    template <class T>
    std::string EnumHelper<T>::toString(T _enum)
    {
        HJARTA_ASSERT_FCT(Implemented, "EnumHelper <", HJARTA_STRINGIFY(T), "> is not implemented");
        return "non-stringified";
    }

    //------------------------------------------------------------------------------------------
    
    template <class T>
    T EnumHelper<T>::toEnum(const std::string& _str)
    {
        HJARTA_ASSERT_FCT(Implemented, "EnumHelper <", HJARTA_STRINGIFY(T), "> is not implemented");
        return T{};
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    int EnumHelper<T>::toIndex(T _enum)
    {
        HJARTA_ASSERT_FCT(Implemented, "EnumHelper <", HJARTA_STRINGIFY(T), "> is not implemented");
        return int(_enum);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    int EnumHelper<T>::toIndex(const std::string& _str)
    {
        HJARTA_ASSERT_FCT(Implemented, "EnumHelper <", HJARTA_STRINGIFY(T), "> is not implemented");
        return int(toEnum(_str));
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void EnumSerializable<T>::serialize(Serializer& _serializer)
    {
        std::string str = EnumHelper<T>::toString(*m_value);
        _serializer.serializeSimple(&str);

        if (_serializer.isUnserializing())
        {
            *m_value = EnumHelper<T>::toEnum(str);
        }
    }
}

#endif // HJARTA_Enum_inl