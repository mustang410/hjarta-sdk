#ifndef HJARTA_Board_inl
#define HJARTA_Board_inl

namespace hjarta
{
    template <class T>
    void Board::setValue(const StringId& _id, std::map<StringId, T>& _map, T _value)
    {
        _map[_id] = _value;
        sendEvent<BoardEvent>(_id, _value);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    T Board::getValue(const StringId& _id, const std::map<StringId, T>& _map, T _default) const
    {
        auto it = _map.find(_id);
        if (it != _map.end())
        {
            return it->second;
        }
        return _default;
    }
}

#endif // HJARTA_Board_inl