#ifndef _HJARTA_SERIALIZATION_STRING_ID_H_
#define _HJARTA_SERIALIZATION_STRING_ID_H_

#include "hjarta/core/CRC32.h"

#include <string>

namespace hjarta
{
    // ## StringId
    // Unique id based on a string value
    class StringId
    {
    public:
        /// ##### Custom types
        using ValueType = CRC32::ValueType;
        static const ValueType InvalidValue;

    public:
        StringId() = default;
        StringId(const char* _friendly);
        StringId(const std::string& _friendly);
        StringId& operator=(const std::string& _friendly);
        void set(const char* _friendly);

    public:
        /// ##### StringId interface
        bool                isValid() const { return m_value != InvalidValue; }
        const std::string&  getFriendly() const { return m_friendly; }
        ValueType           getValue() const { return m_value; }

    private:
        /// ##### Attributes
        std::string         m_friendly;
        ValueType           m_value = InvalidValue;
    };

    bool operator<(const StringId& _left, const StringId& _right);
    bool operator==(const StringId& _left, const StringId& _right);
}

namespace std
{
    inline string to_string(const hjarta::StringId& _stringId) { return _stringId.getFriendly(); }
}

#endif // _HJARTA_SERIALIZATION_STRING_ID_H_