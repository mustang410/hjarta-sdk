#include "hjarta/core/Board.h"

namespace hjarta
{
    bool BoardEvent::asBoolean(bool _default) const
    {
        if (HJARTA_VERIFY(m_type == Boolean, "Board value is not a boolean"))
        {
            return m_asNumeric.m_boolean;
        }
        return _default;
    }

    //------------------------------------------------------------------------------------------

    unsigned BoardEvent::asUnsigned(unsigned _default) const
    {
        if (HJARTA_VERIFY(m_type == Unsigned, "Board value is not an unsigned"))
        {
            return m_asNumeric.m_unsigned;
        }
        return _default;
    }

    //------------------------------------------------------------------------------------------

    int BoardEvent::asSigned(int _default) const
    {
        if (HJARTA_VERIFY(m_type == Signed, "Board value is not a signed"))
        {
            return m_asNumeric.m_signed;
        }
        return _default;
    }

    //------------------------------------------------------------------------------------------

    const std::string& BoardEvent::asString(const std::string& _default) const
    {
        if (HJARTA_VERIFY(m_type == String, "Board value is not a string"))
        {
            return m_asString;
        }
        return _default;
    }

    //------------------------------------------------------------------------------------------

    void Board::serialize(Serializer& _serializer)
    {
        _serializer.serializeMap("booleans", &m_booleans, Serializer::Optional);
        _serializer.serializeMap("unsigned", &m_unsigned, Serializer::Optional);
        _serializer.serializeMap("signed", &m_signed, Serializer::Optional);
        _serializer.serializeMap("strings", &m_strings, Serializer::Optional);
    }
}