#ifndef HJARTA_Callable_inl
#define HJARTA_Callable_inl

namespace hjarta
{
    template <class ...Args>
    Callable<Args...>::Callable(const Callable& _copy)
    {
        m_type = _copy.m_type;
        if (m_type != Callable::Type::Empty)
        {
            m_context = _copy.m_context->clone();
        }
    }

    //------------------------------------------------------------------------------------------

    template <class ...Args>
    Callable<Args...>& Callable<Args...>::operator=(const Callable<Args...>& _copy)
    {
        clear();
        m_type = _copy.m_type;
        if (m_type != Callable::Type::Empty)
        {
            m_context = _copy.m_context->clone();
        }
        return *this;
    }

    //------------------------------------------------------------------------------------------

    template <class ...Args>
    void Callable<Args...>::set(const Lambda& _lambda)
    {
        clear();
        m_context = new LambdaContext(_lambda);
        m_type = Type::Lambda;
    }

    //------------------------------------------------------------------------------------------

    template <class ...Args>
    template <class T, class U>
    void Callable<Args...>::set(T* _object, void(U::*_callback)(Args...))
    {
        clear();
        m_context = new CallbackContext<T, U>(_object, _callback);
        m_type = Type::Callback;
    }

    //------------------------------------------------------------------------------------------

    template <class ...Args>
    void Callable<Args...>::clear()
    {
        if (m_context != nullptr)
        {
            delete m_context;
            m_context = nullptr;
        }

        m_type = Callable::Type::Empty;
    }

    //------------------------------------------------------------------------------------------

    template <class ...Args>
    void Callable<Args...>::call(Args ..._args)
    {
        if (m_context != nullptr)
        {
            m_context->call(std::forward<Args>(_args)...);
        }
    }
}

#endif // HJARTA_Callable_inl