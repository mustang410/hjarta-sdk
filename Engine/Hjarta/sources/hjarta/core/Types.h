#ifndef HJARTA_Types_h
#define HJARTA_Types_h

#include "hjarta\serialization\Serializer.h"

#include "SDL.h"

namespace hjarta
{
    // ### Input
    using ScanCode = SDL_Scancode;
    using Button = Uint8;

    //------------------------------------------------------------------------------------------

    // ### Time
    using Millisecond = unsigned;
    const Millisecond InvalidMs = (unsigned) - 1;

    //------------------------------------------------------------------------------------------

    // ### Point2D
    template <class T>
    struct Point2D : public Serializable
    {
        /// ##### Serialization
        void serialize(Serializer &_serializer)
        {
            _serializer.serializeObject("x", &x);
            _serializer.serializeObject("y", &y);
        }

        Point2D() : x(0), y(0) {}
        Point2D(const T _x, const T _y) : x(_x), y(_y) {}
        bool operator==(const Point2D &_point) const { return x == _point.x && y == _point.y; }
        bool operator!=(const Point2D &_point) const { return x != _point.x || y != _point.y; }

        T x, y;
    };

    //------------------------------------------------------------------------------------------

    // ### Size2D
    template <class T>
    struct Size2D : public Serializable
    {
        /// ##### Serialization
        void serialize(Serializer &_serializer)
        {
            _serializer.serializeObject("w", &w);
            _serializer.serializeObject("h", &h);
        }

        Size2D() : w(0), h(0) {}
        Size2D(const T _w, const T _h) : w(_w), h(_h) {}
        bool operator==(const Size2D &_size) const { return w == _size.w && h == _size.h; }
        bool operator!=(const Size2D &_size) const { return w != _size.w || h != _size.h; }

        Size2D<T> buildInnerSize(T _dw, T _dh);
        Size2D<T> buildOuterSize(T _dw, T _dh);

        T w, h;
    };

    //------------------------------------------------------------------------------------------

    // ### Vector2D
    template <class T>
    struct Vector2D : public Serializable
    {
        /// ##### Serialization
        void serialize(Serializer &_serializer)
        {
            _serializer.serializeObject("x", &x);
            _serializer.serializeObject("y", &y);
        }

        Vector2D() : x(0), y(0) {}
        Vector2D(const T &_x, const T &_y) : x(_x), y(_y) {}
        bool operator==(const Vector2D &_vector) const { return x == _vector.x && y == _vector.y; }
        bool operator!=(const Vector2D &_vector) const { return x != _vector.x || y != _vector.y; }


        T x, y;
    };

    //------------------------------------------------------------------------------------------

    // ### Rect2D
    template <class T, class U>
    struct Rect2D : public Serializable
    {
        /// ##### Serialization
        void serialize(Serializer &_serializer)
        {
            point.serialize(_serializer);
            size.serialize(_serializer);
        }

        Rect2D() {}
        Rect2D(const SDL_Rect& _rect) : point(T(_rect.x), T(_rect.y)), size(U(_rect.w), U(_rect.h)) {}
        Rect2D(const Point2D<T>& _point, const Size2D<U>& _size) : point(_point), size(_size) {}
        Rect2D(const T& _x, const T& _y, const U& _w, const U& _h) : point(_x, _y), size(_w, _h) {}
        bool operator==(const Rect2D<T, U>& _rect) const { return point == _rect.point && size == _rect.size; }
        bool operator!=(const Rect2D<T, U>& _rect) const { return point != _rect.point || size != _rect.size; }

        /// ##### Utilities
        Rect2D<T, U>    rescale(int _offsetX, int _offsetY, double _factorW, double _factorH) const;
        Rect2D<T, U>    inter(const Rect2D<T, U>& _rect) const;
        bool            contains(const Point2D<T>& _point) const;

        /// ##### Convert
        SDL_Rect        toSDLRect() const { return { int(point.x), int(point.y), int(size.w), int(size.h) }; }

        Point2D<T>      point;
        Size2D<U>       size;
    };

    //------------------------------------------------------------------------------------------

    // ### Common types
    using Path = std::string;
    using Px = int;
    using SizePx = Size2D<Px>;
    using PointPx = Point2D<Px>;
    using RectPx = Rect2D<Px, Px>;
}

#include "hjarta/core/Types.inl"

#endif // HJARTA_Types_h