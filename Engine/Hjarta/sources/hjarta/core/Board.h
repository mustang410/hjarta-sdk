#ifndef HJARTA_Board_h
#define HJARTA_Board_h

#include "hjarta/core/StringId.h"
#include "hjarta/serialization/Serializer.h"
#include "hjarta/event/Event.h"
#include "hjarta/event/EventDispatcher.h"

#include <map>
#include <string>

namespace hjarta
{
    // ## BoardEvent
    // Event sent when there is a board update
    class BoardEvent : public Event
    {
        HJARTA_DECL_EVENT(hjarta::BoardEvent, hjarta::Event)

    public:
        /// ##### Custom types
        union Value
        {
            Value() = default;
            Value(bool _boolean) : m_boolean(_boolean) {}
            Value(unsigned _unsigned) : m_unsigned(_unsigned) {}
            Value(int _signed) : m_signed(_signed) {}
            bool        m_boolean;
            unsigned    m_unsigned;
            int         m_signed;
        };
        enum Type { Empty, Boolean, Unsigned, Signed, String };

    public:
        BoardEvent() = default;
        BoardEvent(const hjarta::StringId& _id, bool _boolean) : m_valueId(_id), m_type(Boolean), m_asNumeric(_boolean) {}
        BoardEvent(const hjarta::StringId& _id, unsigned _unsigned) : m_valueId(_id), m_type(Unsigned), m_asNumeric(_unsigned) {}
        BoardEvent(const hjarta::StringId& _id, int _signed) : m_valueId(_id), m_type(Signed), m_asNumeric(_signed) {}
        BoardEvent(const hjarta::StringId& _id, const std::string& _string) : m_valueId(_id), m_type(String), m_asString(_string) {}

    public:
        /// ##### Event interface
        const hjarta::StringId&         getValueId() const { return m_valueId; }
        Type                            getType() const { return m_type; }
        bool                            asBoolean(bool _default = bool{}) const;
        unsigned                        asUnsigned(unsigned _default = unsigned{}) const;
        int                             asSigned(int _default = int{}) const;
        const std::string&              asString(const std::string& _default = std::string{}) const;

    private:
        /// ##### Attribute
        StringId                        m_valueId;
        Value                           m_asNumeric;
        std::string                     m_asString;
        Type                            m_type = Empty;
    };

    // ## Board
    // A Board handle a set of values mapped to an id
    // When a value is updated, the board send event related to the value
    class Board : public EventDispatcher, public Serializable
    {
    public:
        /// ##### Serialization
        void                            serialize(Serializer& _serializer) override;

    public:
        /// ##### Board interface
        // setters
        void                            setBoolean(const StringId& _id, bool _value) { setValue(_id, m_booleans, _value); }
        void                            setUnsigned(const StringId& _id, unsigned _value) { setValue(_id, m_unsigned, _value); }
        void                            setSigned(const StringId& _id, int _value) { setValue(_id, m_signed, _value); }
        void                            setString(const StringId& _id, const std::string& _value) { setValue(_id, m_strings, _value); }
        // getters
        bool                            getBoolean(const StringId& _id, bool _default = bool{}) const { return getValue(_id, m_booleans, _default); }
        unsigned                        getUnsigned(const StringId& _id, unsigned _default = unsigned{}) const { return getValue(_id, m_unsigned, _default); }
        int                             getSigned(const StringId& _id, int _default = int{}) const { return getValue(_id, m_signed, _default); }
        const std::string&              getString(const StringId& _id, const std::string& _default = std::string{}) const { return getValue(_id, m_strings, _default); }

    private:
        /// ##### Helpers
        template <class T>
        void                            setValue(const StringId& _id, std::map<StringId, T>& _map, T _value);
        template <class T>
        T                               getValue(const StringId& _id, const std::map<StringId, T>& _map, T _default) const;

    private:
        /// ##### Attributes
        std::map<StringId, bool>        m_booleans;
        std::map<StringId, unsigned>    m_unsigned;
        std::map<StringId, int>         m_signed;
        std::map<StringId, std::string> m_strings;
    };
}

#include "hjarta/core/Board.inl"

#endif // HJARTA_Board_h