#ifndef HJARTA_Enum_h
#define HJARTA_Enum_h

#include "hjarta/serialization/Serializer.h"
#include "hjarta/core/StringFacility.h"

#include <vector>
#include <array>
#include <string>

namespace hjarta
{
    // #### EnumType
    // Internal type to store additional information for enums
    template <class EnumType>
    struct EnumValue
    {
        constexpr EnumValue() = default;
        constexpr EnumValue(EnumType _enum, const char* _friendly) : m_enum(_enum), m_friendly(_friendly) {}
        EnumType                m_enum;
        const char*             m_friendly;
    };

    // ## EnumHelper
    // Default generic templated helper class to provide stringification tools for enums
    // Only template specialization using the macro below can activate this feature for enums
    template <class T>
    class EnumHelper
    {
    public:
        static constexpr bool   Implemented = false;
        static constexpr size_t Count = 0;
        static constexpr std::array<EnumValue<T>, Count> Values = std::array<EnumValue<T>, Count>{};

    public:
        static constexpr std::array<T, Count> toRange() { return std::array<T, Count> {}; }
        // Get string value for enum value
        static std::string      toString(T _enum);
        // Get enum value for string value
        static T                toEnum(const std::string& _str);
        // Cat enum value to enum underlyingtype
        static int              toIndex(T _enum);
        static int              toIndex(const std::string& _str);
    };

    // ## EnumSerializable
    // Helper used by Serializer to serialize enum
    // Enum has to declare its helper using the HJARTA_DECL_ENUM_HELPER macro
    template <class T>
    class EnumSerializable : public Serializable
    {
    public:
        /// ##### Serialization
        void serialize(Serializer& _serializer) override;

    public:
        static_assert(EnumHelper<T>::Implemented, "Enum is not serializable");
        EnumSerializable(T* _value) : m_value(_value) {}

    private:
        /// ##### Attributes
        T* m_value;
    };
}

// ## HJARTA_DECL_ENUM_HELPER
// Declare utility tools for the specified enum
// #### This macro has to be called outside any namespace
// #### Declared enum has to have its last member named Count
// Use EnumHelper<EnumType> to retrieve utility tool for the enum once this macro has been used
// This macro require the enum type and a map mapping each value to a string value
#define HJARTA_DECL_ENUM_HELPER(EnumType, ...)                                          \
namespace hjarta                                                                        \
{                                                                                       \
    template <>                                                                         \
    class EnumHelper<EnumType>                                                          \
    {                                                                                   \
    public:                                                                             \
        static constexpr bool Implemented = true;                                       \
        static constexpr size_t Count = size_t(EnumType::Count);                        \
        static constexpr std::array<EnumValue<EnumType>, Count> Values = std::array<EnumValue<EnumType>, Count>{__VA_ARGS__};    \
                                                                                        \
    public:                                                                             \
        static constexpr std::array<EnumType, Count> toRange()                          \
        {                                                                               \
            std::array<EnumType, Count> range;                                      \
            size_t index = 0;                                                           \
            for (const EnumValue<EnumType>& value : Values)                             \
                range[index++] = value.m_enum;                                          \
            return range;                                                               \
        }                                                                               \
        static std::string toString(EnumType _enum)                                     \
        {                                                                               \
            for (const EnumValue<EnumType>& value : Values)                             \
            {                                                                           \
                if (value.m_enum == _enum)                                              \
                    return value.m_friendly;                                            \
            }                                                                           \
            HJARTA_LOG_FCT_ERROR("Unknown enum value for EnumHelper<", HJARTA_STRINGIFY(EnumType), ">"); \
            return "non-stringified";                                                   \
        }                                                                               \
        static EnumType toEnum(const std::string& _str)                                 \
        {                                                                               \
            for (const EnumValue<EnumType>& value : Values)                             \
            {                                                                           \
                if (value.m_friendly == _str)                                           \
                    return value.m_enum;                                                \
            }                                                                           \
            HJARTA_LOG_FCT_ERROR("Unknown string value for EnumHelper<", HJARTA_STRINGIFY(EnumType), ">"); \
            return EnumType{};                                                          \
        }                                                                               \
        static int toIndex(EnumType _enum) { return int(_enum); }                       \
        static int toIndex(const std::string& _str) { return int(toEnum(_str)); }       \
    };                                                                                  \
}                                                                                       \

#include "hjarta/core/Enum.inl"

#endif // HJARTA_Enum_h