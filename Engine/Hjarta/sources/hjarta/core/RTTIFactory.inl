#ifndef HJARTA_RTTIFactory_inl
#define HJARTA_RTTIFactory_inl

namespace hjarta
{
    template <class T>
    void RTTIFactory::registerClass()
    {
        static_assert(std::is_base_of<RTTI, T>::value, "Class has to inherit from RTTI");
        m_idBuilders[T::getClassId()] = &T::buildClass;
        m_nameBuilders[T::getClassName()] = &T::buildClass;
    }
}

#endif // HJARTA_RTTIFactory_inl