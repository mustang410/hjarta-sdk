#ifndef HJARTA_RTTI_inl
#define HJARTA_RTTI_inl

namespace hjarta
{
    template <class T>
    T* RTTI::dynamicCast()
    {
        if (isClass(T::getClassId()))
        {
            return (T*)this;
        }
        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    const T* RTTI::dynamicCast() const
    {
        if (isClass(T::getClassId()))
        {
            return (T*)this;
        }
        return nullptr;
    }
}

#endif // HJARTA_RTTI_inl