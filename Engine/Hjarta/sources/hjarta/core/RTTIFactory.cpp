#include "hjarta/core/RTTIFactory.h"
#include "hjarta/tools/Log.h"

namespace hjarta
{
    RTTI* RTTIFactory::buildClass(RTTI::TypeId _classId)
    {
        auto it = m_idBuilders.find(_classId);
        if (it != m_idBuilders.end())
        {
            return (*it->second)();
        }
        else
        {
            HJARTA_LOG_ERROR("Class id <", _classId, "> is not registered into RTTIFactory <", m_id, ">");
        }

        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    RTTI* RTTIFactory::buildClass(std::string _className)
    {
        auto it = m_nameBuilders.find(_className);
        if (it != m_nameBuilders.end())
        {
            return (*it->second)();
        }
        else
        {
            HJARTA_LOG_ERROR("Class name <", _className, "> is not registered into RTTIFactory <", m_id, ">");
        }

        return nullptr;
    }
}