#ifndef HJARTA_CRC32_h
#define HJARTA_CRC32_h

#include <string.h>

namespace hjarta
{
    // ## CRC32
    // CRC 32 computing utilities
    // From https://www.novatel.com/assets/Documents/Bulletins/apn030.pdf
    class CRC32
    {
    private:
        /// ##### Configuration
        static constexpr int Polynomial = 0xEDB88320;
        static constexpr size_t ByteCount = 8;

    public:
        /// ##### Algorithms
        using ValueType = unsigned long;
        static constexpr ValueType computeValue(int _value)
        {
            int j = 0;
            ValueType crc = _value;
            for (j = ByteCount; j > 0; j--)
            {
                if (crc & 1)
                    crc = (crc >> 1) ^ Polynomial;
                else 
                    crc >>= 1;
            }
            return crc;
        }
        static constexpr ValueType computeBlockValue(const char *_buffer, size_t _len)
        {
            char* ptr = const_cast<char*>(_buffer);
            ValueType crc = 0;
            while (_len-- != 0)
            {
                ValueType chunk1 = (crc >> 8) & 0x00FFFFFFL;
                ValueType chunk2 = computeValue(((int)crc ^ *ptr++) & 0xFF);
                crc = chunk1 ^ chunk2;
            }
            return crc;
        }

    public:
        /// ##### Helpers
        static constexpr ValueType computeStringValue(const char* _buffer) { return computeBlockValue(_buffer, strlen(_buffer)); }
    };
}

#endif // HJARTA_CRC32_h