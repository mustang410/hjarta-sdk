#include "hjarta/core/StringFacility.h"

#include <locale>
#include <codecvt>

namespace hjarta
{
    std::wstring StringFacility::toWideString(const std::string &_input)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
        return converter.from_bytes(_input.c_str());
    }

    //------------------------------------------------------------------------------------------

    std::string StringFacility::toString(const std::wstring &_input)
    {
        ;
        std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
        return converter.to_bytes(_input);
    }

    //------------------------------------------------------------------------------------------

    std::vector<std::string> StringFacility::split(const std::string& _string, char _delimiter)
    {
        return split(_string, std::string(1, _delimiter));
    }

    //------------------------------------------------------------------------------------------

    std::vector<std::string> StringFacility::split(const std::string& _string, const std::string& _delimiter)
    {
        std::vector<std::string> tokens;
        size_t index, offset = 0;
        do
        {
            index = _string.find(_delimiter, offset);
            if (index != std::string::npos)
            {
                tokens.emplace_back(_string.substr(offset, index - offset));
                offset = index + 1;
            }
            else
            {
                tokens.emplace_back(_string.substr(offset));
            }
        } while (index != std::string::npos);
        return tokens;
    }
}