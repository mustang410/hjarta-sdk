#include "hjarta/core/StringId.h"

namespace hjarta
{
    const StringId::ValueType StringId::InvalidValue = 0;

    //------------------------------------------------------------------------------------------

    StringId::StringId(const char* _friendly)
    {
        set(_friendly);
    }

    //------------------------------------------------------------------------------------------

    StringId::StringId(const std::string& _friendly)
    {
        set(_friendly.c_str());
    }

    //------------------------------------------------------------------------------------------

    StringId& StringId::operator=(const std::string& _friendly)
    {
        set(_friendly.c_str());
        return *this;
    }

    //------------------------------------------------------------------------------------------

    void StringId::set(const char* _friendly)
    {
        m_friendly = _friendly;
        m_value = CRC32::computeStringValue(_friendly);
    }

    //------------------------------------------------------------------------------------------

    bool operator<(const StringId& _left, const StringId& _right)
    {
        return _left.getValue() < _right.getValue();
    }

    //------------------------------------------------------------------------------------------

    bool operator==(const StringId& _left, const StringId& _right)
    {
        return _left.getValue() == _right.getValue();
    }
}