#ifndef HJARTA_InstanceProvider_h
#define HJARTA_InstanceProvider_h

namespace hjarta
{
    // ## InstanceProvider
    // Singleton template handling
    template <class T>
    class InstanceProvider
    {
    public:
        /// ##### Provider interface
        template <class ...Args>
        static void create(Args&& ..._args);
        static void assign(T* _instance, bool _owner = false);
        static bool exists() { return ms_instance != nullptr; }
        static void clear();
        static T*   getPtr() { return ms_instance; }
        static T&   get() { return *ms_instance; }

    private:
        /// ##### Attributes
        static T*   ms_instance;
        static bool ms_owner;
    };

    // ## AutoInstanceProvider
    // Autofill the InstanceProvider during the lifetime of this object
    template <class T>
    class AutoInstanceProvider
    {
        template <class ...Args>
        AutoInstanceProvider(Args&& ..._args);
        ~AutoInstanceProvider();
    };
}

#include "hjarta/core/InstanceProvider.inl"

#endif // HJARTA_InstanceProvider_h