#ifndef HJARTA_Callable_h
#define HJARTA_Callable_h

#include "hjarta/core/RTTI.h"

#include <functional>

namespace hjarta
{
    // ## Callable
    // Handle either a lambda or an object callback
    // Objects have to inherit from base ICallable type
    template <class ...Args>
    class Callable
    {
    public:
        /// ##### Custom types
        enum class Type { Empty, Lambda, Callback };
        using Lambda = std::function<void(Args...)>;

    public:
        Callable() = default;
        Callable(Lambda&& _lambda) { set(_lambda); }
        template <class T, class U>
        Callable(T* _object, void(U::*_callback)(Args...)) { set(_object, _callback); }
        Callable(const Callable& _copy);
        Callable& operator=(const Callable<Args...>& _copy);
        ~Callable() { clear(); }
        operator bool() const { return m_type != Type::Empty; }
        void operator()(Args ..._args) { call(std::forward<Args>(_args)...); }

    public:
        /// ##### Callable interface
        void        set(const Lambda& _lambda);
        template <class T, class U>
        void        set(T* _object, void(U::*_callback)(Args...));
        void        clear();
        void        call(Args ..._args);

    private:
        /// ##### Context
        // Base context
        struct IContext : public RTTI
        {
            HJARTA_DECL_RTTI_ABSTRACT(IContext, RTTI)
            virtual void call(Args&& ..._args) = 0;
            virtual IContext* clone() = 0;
        };
        // Callback - Object dedicated context
        template <class T, class U>
        struct CallbackContext : public IContext
        {
            HJARTA_DECL_RTTI(CallbackContext, IContext)
            CallbackContext(T* _object, void(U::*_callback)(Args...)) : m_object(_object), m_callback(_callback) {}
            void call(Args&& ..._args) override { (m_object->*m_callback)(std::forward<Args>(_args)...); }
            IContext* clone() override { return new CallbackContext<T, U>(m_object, m_callback); }
            T* m_object = nullptr;
            void(U::*m_callback)(Args...) = nullptr;
        };
        // Lambda context
        struct LambdaContext : public IContext
        {
            HJARTA_DECL_RTTI(LambdaContext, IContext)
            LambdaContext(const Lambda& _lambda) : m_lambda(_lambda) {}
            void call(Args&& ..._args) { m_lambda(std::forward<Args>(_args)...); }
            IContext* clone() override { return new LambdaContext(m_lambda); }
            Lambda m_lambda = nullptr;
        };

    private:
        /// ##### Attributes
        Type        m_type = Type::Empty;
        IContext*   m_context = nullptr;
    };
}

#include "hjarta/core/Callable.inl"

#endif //HJARTA_Callable_h