#ifndef HJARTA_Types_inl
#define HJARTA_Types_inl

#include "hjarta/tools/Log.h"

#include <algorithm>

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

namespace hjarta
{
    template <class T>
    Size2D<T> Size2D<T>::buildInnerSize(T _dw, T _dh)
    {
        Size2D<T> size = *this;
        size.w -= _dw;
        size.h -= _dh;
        return size;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    Size2D<T> Size2D<T>::buildOuterSize(T _dw, T _dh)
    {
        Size2D<T> size = *this;
        size.w += _dw;
        size.h += _dh;
        return size;
    }

    //------------------------------------------------------------------------------------------

    template <class T, class U>
    Rect2D<T, U> Rect2D<T, U>::rescale(int _offsetX, int _offsetY, double _factorW, double _factorH) const
    {
        return Rect2D<T, U>(Point2D<T>(point.x + _offsetX, point.y + _offsetY), Size2D<U>(U(size.w * _factorW), U(size.h * _factorH)));
    }

    //------------------------------------------------------------------------------------------

    template <class T, class U>
    Rect2D<T, U> Rect2D<T, U>::inter(const Rect2D<T, U>& _rect) const
    {
        if ((point.x + size.w < _rect.point.x)
            || (point.x >= _rect.point.x + _rect.size.w)
            || (point.y + size.h < _rect.point.y)
            || (point.y >= _rect.point.y + _rect.size.h))
            return Rect2D<T, U>(); // no intersection

        Rect2D<T, U> intersection;

        T left = std::max(point.x, _rect.point.x);
        T right = std::min(point.x + size.w, _rect.point.x + _rect.size.w);
        T bottom = std::min(point.y + size.h, _rect.point.y + _rect.size.h);
        T top = std::max(point.y, _rect.point.y);

        intersection.point.x = left;
        intersection.point.y = top;
        intersection.size.w = U(right - left);
        intersection.size.h = U(bottom - top);
        return intersection;
    }

    //------------------------------------------------------------------------------------------

    template <class T, class U>
    bool Rect2D<T, U>::contains(const Point2D<T>& _point) const
    {
        return (_point.x >= point.x && _point.x <= T(point.x + size.w)
            && _point.y >= point.y && _point.y <= T(point.y + size.h));
    }
}

#endif // HJARTA_Types_inl