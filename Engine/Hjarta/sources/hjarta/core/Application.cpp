#include "hjarta/core/Application.h"
#include "hjarta/core/Types.h"
#include "hjarta/tools/Log.h"
#include "hjarta/tools/Profiler.h"
#include "hjarta/core/RTTIFactory.h"
#include "hjarta/input/InputManager.h"
#include "hjarta/event/EventDispatcher.h"
#include "hjarta/data/ResourceManager.h"

namespace hjarta
{
    void Application::init(const Size2D<int>& _size, const std::string& _title, const int _fps)
    {
        // saving settings
        m_size = _size;
        m_title = _title;
        setFPS(_fps);

        // SDL
        if (initSDL() == -1)
        {
            // initialization error
            HJARTA_LOG_ERROR("Unable to initialize SDL");
        }
        else
        {
            initModules();
            
            // initialization done
            HJARTA_LOG_INFO("SDL successfuly initialized ");
            m_initialized = true;
        }
    }

    //------------------------------------------------------------------------------------------

    void Application::terminate()
    {
        terminateModules();

        // destroying object
        m_renderer.destroy();
        SDL_DestroyWindow(m_win);

        // quit SDL
        TTF_Quit();
        SDL_Quit();
        m_initialized = false;
    }

    //------------------------------------------------------------------------------------------

    int Application::initSDL()
    {
        // SDL
        if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
        {
            // initialization error
            HJARTA_LOG_INFO("Error occured during the SDL initialization");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
            terminate(); // quit SDL
            return -1;
        }
        // TTF
        if (TTF_Init() == -1)
        {
            // initialization error
            HJARTA_LOG_INFO("Error occured during the TTF initialization");
            HJARTA_LOG_INFO("TTF error: ", TTF_GetError());
            terminate(); // quit SDL
            return -1;
        }
        // window
        m_win = SDL_CreateWindow(m_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_size.w, m_size.h, 0);
        if (m_win == nullptr)
        {
            // windows creation error
            HJARTA_LOG_INFO("Error occured during the window creation");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
            terminate(); // quit SDL
            return -1;
        }
        // renderer
        if (!m_renderer.init(m_win))
        {
            // renderer creation error
            HJARTA_LOG_INFO("Error occured during the renderer creation");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
            terminate(); // quit SDL
            return -1;
        }

        return 0;
    }

    //------------------------------------------------------------------------------------------

    void Application::initModules()
    {
        InstanceProvider<RTTIFactory>::create("GlobalRTTIFactory");
        InstanceProvider<Log>::create();
        InstanceProvider<Profiler>::create();
        InstanceProvider<InputManager>::create();
        InstanceProvider<EventDispatcher>::create();

        // ResourceManager
        InstanceProvider<ResourceManager>::create("GlobalResourceManager");
    }

    //------------------------------------------------------------------------------------------

    void Application::terminateModules()
    {
        InstanceProvider<Log>::clear();
        InstanceProvider<Profiler>::clear();
        InstanceProvider<RTTIFactory>::clear();
        InstanceProvider<InputManager>::clear();
        InstanceProvider<EventDispatcher>::clear();

        // ResourceManager
        InstanceProvider<ResourceManager>::clear();
    }

    //------------------------------------------------------------------------------------------

    void Application::setIcon(const std::string &_fileName)
    {
        SDL_Surface *icon = IMG_Load(_fileName.c_str());
        if (icon == nullptr)
        {
            // unable to load the icon
            HJARTA_LOG_INFO("Unable to load window icon");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
        }
        else
        {
            SDL_SetWindowIcon(m_win, icon);
            SDL_FreeSurface(icon);
        }
    }

    //------------------------------------------------------------------------------------------

    void Application::eol(LimitFPS _limitFPS)
    {
        static int prev = SDL_GetTicks();
        // previous loop timer
        int current = SDL_GetTicks();
        int prevLoopDt = current - prev;
        // apply limit if requested
        if (_limitFPS == LimitFPS::Yes && prevLoopDt < m_fpsDtThreshold)
        {
            int dtToSleep = int(m_fpsDtThreshold) - prevLoopDt;
            SDL_Delay(unsigned(dtToSleep));
        }

        // calcul FPS
        current = SDL_GetTicks();
        int buffer = (current - prev);

        if (buffer > 0)
        {
            m_fps = (1000.0 / double(buffer));
        }
        else
        {
            buffer = 0;
            m_fps = 0;
        }
        m_loopDt = (Millisecond)(buffer);

        prev = current;

        HJARTA_PROFILER().eol();
    }

    //------------------------------------------------------------------------------------------

    void Application::fatalErrorOccurred()
    {
        HJARTA_LOG_ERROR("A fatal error has occurred");
        terminate();
        exit(EXIT_FAILURE);
    }
}