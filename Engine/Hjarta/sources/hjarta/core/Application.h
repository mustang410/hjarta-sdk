#ifndef HJARTA_Application_h
#define HJARTA_Application_h

#include "SDL.h"
#include <SDL_ttf.h>
#include <SDL_image.h>

#include "hjarta\core\Types.h"
#include "hjarta\gfx\Renderer.h"

#include <string>
#include <map>

#define HJARTA_APPLICATION()        hjarta::InstanceProvider<hjarta::Application>::get()
#define HJARTA_RENDERER()           HJARTA_APPLICATION().getRenderer()
#define HJARTA_FATAL_ERROR()        HJARTA_APPLICATION().fatalErrorOccurred();

namespace hjarta
{
    // ## Application
    // Handle the SDL initialization and all the main objects needed 
    class Application
    {
    public:
        /// ##### Application interface
        void                        init(const Size2D<int>& _size, const std::string& _title, const int _fps = 60);
        void                        terminate();

    private:
        /// ##### Internal methods
        int                         initSDL();
        void                        initModules();
        void                        terminateModules();

    public:
        /// ##### Getters & setters
        bool                        isInitialized() const { return m_initialized; }
        SDL_Window*                 getWindow() { return m_win; }
        const Size2D<int>           getWindowSize() const { return m_size; }
        void                        setIcon(const std::string& _fileName);

        /// ##### FPS & dt management
        void                        setFPS(const double _fps) { m_fpsDtThreshold = 1000.0 / _fps; }
        double                      getFPS() const { return m_fps; }
        Millisecond                 getLoopDt() const { return m_loopDt; }
        enum class LimitFPS { Yes, No };
        void                        eol(LimitFPS _limitFPS = LimitFPS::Yes); // must be last instruction of each game loop
        Millisecond                 getElapsedMs() const { return SDL_GetTicks(); }

        /// ##### Renderer management
        inline Renderer&            getRenderer() { return m_renderer; }

        /// ##### Other
        void                        fatalErrorOccurred();

    private:
        /// ##### Application settings
        Size2D<int>                 m_size = Size2D<int>(800, 600);
        std::string                 m_title = "HJARTA Engine";
        double                      m_fpsDtThreshold = 60;
        double                      m_fps = 0;

        /// ##### Application internal state
        bool                        m_initialized = false;
        Millisecond                 m_loopDt = 0;
        SDL_Window*                 m_win = nullptr;
        Renderer                    m_renderer;
    };
}

#endif // HJARTA_Application_h