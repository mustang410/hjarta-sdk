#ifndef HJARTA_StringFacility_h
#define HJARTA_StringFacility_h

#include <string>
#include <vector>

#define HJARTA_STRINGIFY(item) #item
#define HJARTA_CLASS_NAME(obj) std::string(typeid(obj).name()).substr(7)

namespace hjarta
{
    class StringFacility
    {
    public:
        /// ##### Wide strings
        static std::wstring             toWideString(const std::string &_input);
        static std::string              toString(const std::wstring &_input);

        /// ##### Processing methods
        static std::vector<std::string> split(const std::string& _string, char _delimiter);
        static std::vector<std::string> split(const std::string& _string, const std::string& _delimiter);
    };
}

#endif // HJARTA_StringFacility_h