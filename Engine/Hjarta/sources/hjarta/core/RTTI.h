#ifndef HJARTA_RTTI_h
#define HJARTA_RTTI_h

#include "hjarta/core/CRC32.h"

#include <cstring>
#include <string.h>
#include <iostream>

// Private macro for RTTI base identifiers
#define _HJARTA_DECL_RTTI_IDENTIFIERS(Type)                         \
    public:                                                         \
        static const char*  getClassName() { return #Type; }        \
        static TypeId       getClassId() { return hjarta::CRC32::computeStringValue(#Type); } \

namespace hjarta
{
    // ## RTTI
    // RTTI base class and interface
    // Any RTTI object has to declare its implementation using the appropriate macros
    // See https://www.codeproject.com/Tips/1068171/Cplusplus-Custom-RTTI
    class RTTI
    {
    public:
        using TypeId = CRC32::ValueType;
        virtual const char* getClassInstanceName() = 0;
        virtual const TypeId getClassInstanceId() = 0;

        _HJARTA_DECL_RTTI_IDENTIFIERS(RTTI)

        virtual bool        isClass(TypeId _id) { return _id == RTTI::getClassId(); }
        virtual bool        isClass(const char* _name) {  return strcmp(_name, RTTI::getClassName()) == 0; }

        /// ##### Safe cast handling
        template <class T> T* dynamicCast();
        template <class T> const T* dynamicCast() const;
    };
}

// ## HJARTA_DECL_RTTI_ABSTRACT
// Implement RTTI methods for abstract objects
#define HJARTA_DECL_RTTI_ABSTRACT(Type, ParentType)                 \
    public:                                                         \
        using Parent = ParentType;                                  \
        virtual const char* getClassInstanceName() override { return Type::getClassName(); } \
        virtual const TypeId getClassInstanceId() override { return Type::getClassId(); } \
                                                                    \
        _HJARTA_DECL_RTTI_IDENTIFIERS(Type)                         \
                                                                    \
        virtual bool isClass(TypeId _id) override                   \
        {                                                           \
            if (_id == getClassId())                                \
                return true;                                        \
            else                                                    \
                return Parent::isClass(_id);                        \
        }                                                           \
                                                                    \
        bool virtual isClass(const char* _name) override            \
        {                                                           \
            if (strcmp(_name, getClassName()) == 0)                 \
                return true;                                        \
            else                                                    \
                return Parent::isClass(_name);                      \
        }                                                           \

// ## HJARTA_DECL_RTTI
// Implement all RTTI methods
#define HJARTA_DECL_RTTI(Type, ParentType)                          \
        HJARTA_DECL_RTTI_ABSTRACT(Type, ParentType)                 \
                                                                    \
    public:                                                         \
        static RTTI* buildClass()                                   \
        {                                                           \
            return new Type();                                      \
        }                                                           \

#include "hjarta/core/RTTI.inl"

#endif // HJARTA_RTTI_h