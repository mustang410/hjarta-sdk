#ifndef HJARTA_RTTIFactory_h
#define HJARTA_RTTIFactory_h

#include "hjarta/core/RTTI.h"

#include <map>
#include <functional>

#define HJARTA_RTTI_FACTORY() hjarta::InstanceProvider<hjarta::RTTIFactory>::get()

namespace hjarta
{
    // ## RTTIFactory
    // Used to build RTTI classes
    class RTTIFactory
    {
    public:
        RTTIFactory(const std::string _id) : m_id(_id) {}

    public:
        /// ##### Factory interface
        const std::string&                  getFactoryId() const { return m_id; }
        template <class T> void             registerClass();
        RTTI*                               buildClass(RTTI::TypeId _classId);
        RTTI*                               buildClass(std::string _className);

    private:
        /// ##### Attributes
        std::string m_id;
        typedef RTTI* (*Builder)();
        std::map<RTTI::TypeId, Builder>     m_idBuilders;
        std::map<std::string, Builder>      m_nameBuilders;
    };
}

#include "hjarta/core/RTTIFactory.inl"

#endif // HJARTA_RTTIFactory_h