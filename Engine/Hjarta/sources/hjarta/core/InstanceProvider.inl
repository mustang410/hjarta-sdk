#ifndef HJARTA_InstanceProvider_inl
#define HJARTA_InstanceProvider_inl

namespace hjarta
{
    template <class T>
    T* InstanceProvider<T>::ms_instance = nullptr;
    template <class T>
    bool InstanceProvider<T>::ms_owner = false;

    //------------------------------------------------------------------------------------------

    template <class T>
    template <class ...Args>
    static void InstanceProvider<T>::create(Args&& ..._args)
    { 
        clear();
        ms_instance = new T(std::forward<Args>(_args)...); 
        ms_owner = true; 
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void InstanceProvider<T>::assign(T* _instance, bool _owner)
    {
        clear();
        ms_instance = _instance;
        ms_owner = _owner;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void InstanceProvider<T>::clear()
    { 
        if (ms_owner)
            delete ms_instance;
        ms_instance = nullptr; 
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    template <class ...Args>
    AutoInstanceProvider<T>::AutoInstanceProvider(Args&& ..._args)
    {
        InstanceProvider<T>::create(std::forward<Args>(_args)...);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    AutoInstanceProvider<T>::~AutoInstanceProvider()
    {
        InstanceProvider<T>::clear();
    }
}

#endif // HJARTA_InstanceProvider_inl