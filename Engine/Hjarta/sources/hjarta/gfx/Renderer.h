#ifndef _HJARTA_CORE_RENDERER_H_
#define _HJARTA_CORE_RENDERER_H_

#include "hjarta/core/Types.h"
#include "hjarta/core/StringId.h"
#include "hjarta/gfx/Color.h"

#include "SDL.h"
#include <vector>

namespace hjarta
{
    // ## Renderer
    // Handle a SDL renderer
    class Renderer
    {
    public:
        Renderer() = default;
        ~Renderer();

    public:
        /// ##### Renderer interface
        bool                            init(SDL_Window *_window);
        void                            destroy();

        inline SDL_Renderer*            get() { return m_ren; }

        /// ##### Main
        void                            clear(const Color &_color = colors::Invisible);
        void                            present();

        /// ##### Low-level Rendering
        const PointPx&                  getOffset() const;
        void                            copy(SDL_Texture *_tex, const SDL_Rect *_src, const SDL_Rect *_dst);
        void                            copyEx(SDL_Texture *_tex, const SDL_Rect *_src, const SDL_Rect *_dst, const double _angle, const SDL_Point *_center, const SDL_RendererFlip _flip);
        /// ##### Debug draw
        void                            drawLine(const PointPx& _a, const PointPx& _b);
        void                            fillRect(const RectPx& _rect);

        /// ##### Renderer controls
        void                            pushColor(const Color &_color);
        void                            popColor();

        void                            pushClipRect(const RectPx &_cliprect);
        void                            popClipRect();
        void                            pushTarget(SDL_Texture *_target, const PointPx &_offset);
        void                            popTarget();

        /// ##### Statistics
        unsigned                        getCopyPerFrame() const { return m_copyPerFrame; }
        unsigned                        getDrawPerFrame() const { return m_drawPerFrame; }

    private:
        /// ##### Attributes
        SDL_Renderer*                   m_ren = nullptr;
        // statistics
        unsigned                        m_copyPerFrameCounter = 0, m_copyPerFrame;
        unsigned                        m_drawPerFrameCounter = 0, m_drawPerFrame;
        /// ##### Control stacks
        // Color stack
        class ColorRef
        {
        public:
            ColorRef(const Color &_color) : m_color(m_color), m_counter(1) {}

            const Color&                getColor() const { return m_color; }
            void                        increment() { ++m_counter; }
            void                        decrement() { --m_counter; }
            bool                        isReferenced() { return m_counter > 0; }

        private:
            Color                       m_color;
            int                         m_counter = 0;
        };
        // Target stack
        class TargetRef
        {
        public:
            TargetRef(SDL_Texture *_target, const PointPx &_offset) : m_target(_target), m_offset(_offset) {}

            SDL_Texture*                getTarget() const { return m_target; }
            const PointPx&              getOffset() const { return m_offset; }

        private:
            SDL_Texture*                m_target;
            PointPx                     m_offset;
        };

        /// ##### Stacks
        std::vector<ColorRef>           m_colors;
        std::vector<RectPx>             m_cliprects;
        std::vector<TargetRef>          m_targets;
    };
}

#endif // _HJARTA_CORE_RENDERER_H_