#include "hjarta\gfx\Texture.h"

#include "hjarta\core\Application.h"
#include "hjarta/tools/Log.h"

#include "SDL_image.h"

namespace hjarta
{
    void Texture::serialize(Serializer& _serializer)
    {
        _serializer.serializeObject("path", &m_path);
        _serializer.serializeObject("magn", &m_magnification);

        if (_serializer.isUnserializing())
        {
            load(m_path);
        }
    }

    //------------------------------------------------------------------------------------------

    const Path Texture::DefaultPath = "data/gfx/default_texture.png";

    //------------------------------------------------------------------------------------------

    Texture::Texture(const Path& _path) :
        m_errorOccurred(false)
    {
        load(_path);
    }

    //------------------------------------------------------------------------------------------

    Texture::~Texture()
    {
        unload();
    }

    //------------------------------------------------------------------------------------------

    void Texture::load(const Path& _path)
    {
        m_renderer = HJARTA_RENDERER().get();
        m_errorOccurred = false;

        // try to load the texture
        if (!createInternal(_path))
        {
            // error occured
            HJARTA_LOG_ERROR("Unable to load the texture <", _path, ">");
            m_errorOccurred = true;

            // try to load the default texture
            if (!createInternal(DefaultPath))
            {
                // fatal error occured
                HJARTA_LOG_ERROR("Unable to load the default texture <", DefaultPath, ">");
                HJARTA_LOG_ERROR("Data folder may be corrupted");
                HJARTA_FATAL_ERROR();
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void Texture::unload()
    {
        if (!m_symbolic && m_tex != nullptr)
            SDL_DestroyTexture(m_tex);
        m_tex = nullptr;
    }

    //------------------------------------------------------------------------------------------

    void Texture::setMagnification(double _magnification)
    {
        m_magnification = _magnification;
        m_magnSize.w = Px(m_srcSize.w * m_magnification);
        m_magnSize.h = Px(m_srcSize.h * m_magnification);
    }

    //------------------------------------------------------------------------------------------

    void Texture::draw(DrawContext& _context)
    {
        _context.compute({0, 0, m_srcSize.w, m_srcSize.h}, m_magnification);

        // render
        if (m_tex)
        {
            if (_context.requireRenderEx())
                HJARTA_RENDERER().copyEx(m_tex,
                    _context.getSrcRect(), _context.getDstRect(), 
                    _context.getAngle(), _context.getCenter(), _context.getFlip());
            else
                HJARTA_RENDERER().copy(m_tex, 
                    _context.getSrcRect(), _context.getDstRect());
        }

    }

    //------------------------------------------------------------------------------------------

    void Texture::draw(const PointPx& _screenPoint, const PointPx& _anchorPoint, double _angle, double _zx, double _zy, SDL_RendererFlip _flip)
    {
        m_context.set(_screenPoint, _anchorPoint, _angle, _zx, _zy, _flip);
        draw(m_context);
    }

    //------------------------------------------------------------------------------------------

    bool Texture::createInternal(const Path& _path)
    {
        // loads as a SDL surface
        SDL_Surface *buffer = IMG_Load(_path.data());
        // checking loading
        if (buffer == nullptr) return false;

        // convert to a SDL texture
        m_tex = SDL_CreateTextureFromSurface(m_renderer, buffer);
        // checking 
        if (m_tex == nullptr)
        {
            // destroying buffer
            SDL_FreeSurface(buffer);
            return false;
        }

        SDL_QueryTexture(m_tex, NULL, NULL, &m_srcSize.w, &m_srcSize.h);
        m_magnSize.w = Px(m_srcSize.w * m_magnification);
        m_magnSize.h = Px(m_srcSize.h * m_magnification);

        // destroying buffer
        SDL_FreeSurface(buffer);

        m_path = _path;
        m_symbolic = false; // texture has been created here and should be destroyed
        return true;
    }
}