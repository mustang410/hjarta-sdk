#ifndef _HJARTA_GFX_COLOR_H_
#define _HJARTA_GFX_COLOR_H_

#include "hjarta/data/Resource.h"

#include "SDL.h"

namespace hjarta
{
    // ## Color
    // Wrapper over a SDL_Color
    // Add utility methods and serialization
    class Color : public Resource, public SDL_Color
    {
        HJARTA_DECL_RESOURCE_CLONABLE(hjarta::Color, hjarta::Resource)

    public:
        /// ##### Serialization
        void        serialize(hjarta::Serializer &_serializer);

    public:
        /// ##### Color interface
        Color(const Uint8 _r = 0, const Uint8 _g = 0, const Uint8 _b = 0, const Uint8 _a = 255);
        Color(const Color &_copy);
        Color&      setR(const Uint8 _r);
        Color&      setG(const Uint8 _g);
        Color&      setB(const Uint8 _b);
        Color&      setA(const Uint8 _a);
        Color&      setRGBA(const Uint8 _r, const Uint8 _g, const Uint8 _b, const Uint8 _a = 255);
        Color       setR(const Uint8 _r) const;
        Color       setG(const Uint8 _g) const;
        Color       setB(const Uint8 _b) const;
        Color       setA(const Uint8 _a) const;
        Color       setRGBA(const Uint8 _r, const Uint8 _g, const Uint8 _b, const Uint8 _a = 255) const;

        /// ##### Operators
        bool operator==(const Color &_color) const { return (r == _color.r && g == _color.g && b == _color.b && a == _color.a); }
        bool operator!=(const Color &_color) const { return !operator==(_color); }
    };

    namespace colors
    {
        // standard colors
        const Color Invisible(0, 0, 0, 0);
        const Color Black(0, 0, 0, 255);
        const Color White(255, 255, 255, 255);
        const Color Red(255, 0, 0, 255);
        const Color Green(0, 255, 0, 255);
        const Color Blue(0, 0, 255, 255);
        const Color Yellow(255, 255, 0, 255);
        const Color Cyan(0, 255, 255, 255);
        const Color Pink(255, 0, 255, 255);
        // Microsoft Visual Studio colors
        const Color MsvDarkGrey(30, 30, 30, 255);
        const Color MsvGrey(42, 42, 44, 255);
        const Color MsvLightGrey(104, 104, 104, 255);
        const Color MsvBlue(0, 122, 204, 255);
        const Color MsvPurple(134, 95, 197, 255);
        // Visual Assist colors
        const Color VAGreen(189, 183, 107, 255);
        const Color VALightGreen(87, 166, 74, 255);
        const Color VADarkGreen(12, 100, 1, 255);
        const Color VAOrange(255, 128, 0, 255);
        const Color VAYellow(255, 215, 0, 255);

        Color       random();
    }
}

#endif // _HJARTA_GFX_COLOR_H_