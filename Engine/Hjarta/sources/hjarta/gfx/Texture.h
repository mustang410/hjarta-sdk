#ifndef HJARTA_Texture_h
#define HJARTA_Texture_h

#include "hjarta\data\Resource.h"
#include "hjarta\core\Types.h"
#include "hjarta\gfx\Renderer.h"
#include "hjarta\gfx\DrawContext.h"

#include "SDL.h"
#include <string>

namespace hjarta
{
    // ## Texture
    // Handle a SDL texture
    // Create a default texture in case of error
    class Texture : public Resource
    {
        HJARTA_DECL_RESOURCE_NON_CLONABLE(hjarta::Texture, hjarta::Resource)

    public:
        /// ##### Serialization
        void                serialize(Serializer& _serializer) override;

    public:
        /// ##### Constants
        static const Path DefaultPath;

        /// ##### Constructors & destructor
        Texture() = default;
        Texture(const Path& _path);
        ~Texture();

    public:
        /// ##### Loading
        void                load(const Path& _path);
        void                reload() { load(m_path); }
        void                unload();

        /// ##### Texture interface
        bool                isValid() const { return !m_errorOccurred; }
        SDL_Texture*        get() { return m_tex; }
        void                setPath(const Path& _path) { load(m_path); }
        const Path&         getPath() const { return m_path; }
        void                setMagnification(double _magnification);
        double              getMagnification() const { return m_magnification; }
        const SizePx&       getSize() const { return m_magnSize; }

        void                draw(DrawContext& _context);
        void                draw(
            const PointPx& _screenPoint, 
            const PointPx& _anchorPoint = PointPx(0, 0), 
            double _angle = 0, 
            double _zx = 1, double _zy = 1, 
            SDL_RendererFlip _flip = SDL_FLIP_NONE);

    private:
        /// ##### Internals
        bool                    createInternal(const Path &_path);

    private:
        /// ##### Attributes
        SDL_Renderer*           m_renderer = nullptr;
        Path                    m_path;
        bool                    m_symbolic = false;
        SDL_Texture             *m_tex = nullptr;
        double                  m_magnification = 1;
        SizePx                  m_srcSize;
        SizePx                  m_magnSize;
        
        bool                    m_errorOccurred = false;

        mutable DrawContext     m_context; // internal context for optimization
    };
}

#endif // HJARTA_Texture_h