#include "hjarta/gfx/Color.h"

namespace hjarta
{
    void Color::serialize(hjarta::Serializer &_serializer)
    {
        _serializer.serializeObject("r", &r);
        _serializer.serializeObject("g", &g);
        _serializer.serializeObject("b", &b);
        _serializer.serializeObject("a", &a);
    }

    //------------------------------------------------------------------------------------------

    Color::Color(const Uint8 _r, const Uint8 _g, const Uint8 _b, const Uint8 _a)
    {
        setRGBA(_r, _g, _b, _a);
    }

    //------------------------------------------------------------------------------------------

    Color::Color(const Color &_copy)
    {
        r = _copy.r;
        g = _copy.g;
        b = _copy.b;
        a = _copy.a;
    }

    //------------------------------------------------------------------------------------------

    Color &Color::setR(const Uint8 _r)
    {
        r = _r;
        return *this;
    }

    //------------------------------------------------------------------------------------------

    Color &Color::setG(const Uint8 _g)
    {
        g = _g;
        return *this;
    }

    //------------------------------------------------------------------------------------------

    Color &Color::setB(const Uint8 _b)
    {
        b = _b;
        return *this;
    }

    //------------------------------------------------------------------------------------------

    Color &Color::setA(const Uint8 _a)
    {
        a = _a;
        return *this;
    }

    //------------------------------------------------------------------------------------------

    Color &Color::setRGBA(const Uint8 _r, const Uint8 _g, const Uint8 _b, const Uint8 _a)
    {
        r = _r;
        g = _g;
        b = _b;
        a = _a;
        return *this;
    }

    //------------------------------------------------------------------------------------------

    Color Color::setR(const Uint8 _r) const
    {
        Color copy = *this;
        return copy.setR(_r);
    }

    //------------------------------------------------------------------------------------------

    Color Color::setG(const Uint8 _g) const
    {
        Color copy = *this;
        return copy.setG(_g);
    }

    //------------------------------------------------------------------------------------------

    Color Color::setB(const Uint8 _b) const
    {
        Color copy = *this;
        return copy.setB(_b);
    }

    //------------------------------------------------------------------------------------------

    Color Color::setA(const Uint8 _a) const
    {
        Color copy = *this;
        return copy.setA(_a);
    }

    //------------------------------------------------------------------------------------------

    Color Color::setRGBA(const Uint8 _r, const Uint8 _g, const Uint8 _b, const Uint8 _a) const
    {
        Color copy = *this;
        return copy.setRGBA(_r, _g, _b, _a);
    }

    //------------------------------------------------------------------------------------------

    Color colors::random()
    {
        Uint8 r = std::rand() % 255;
        Uint8 g = std::rand() % 255;
        Uint8 b = std::rand() % 255;
        return Color(r, g, b, 255);
    }
}