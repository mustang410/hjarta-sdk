#ifndef _HJARTA_GFX_LAYER_H_
#define _HJARTA_GFX_LAYER_H_

#include "hjarta/core/Types.h"
#include "hjarta/gfx/DrawContext.h"

#include "SDL.h"

namespace hjarta
{
    // ## Layer
    // Used as a buffer to avoid redrawing unchanged objects
    // Setup the drawing callback, which will be called when the Layer will be invalidated
    // Then call the draw method to render the layer
    class Layer
    {
    public:
        /// ##### Custom types
        using OnDraw = std::function<void()>;

    public:
        Layer() = default;
        ~Layer();

        void            setup(const OnDraw& _onDraw);

    public:
        /// ##### Layer interface
        void            invalidate() { m_valid = false; }
        void            dirtify() { m_dirty = true; }
        void            draw(const SDL_Rect& _rect);

    private:
        /// ##### Internal method
        void            create(const SDL_Rect& _rect);
        void            destroy();
        void            render();

    private:
        /// ##### Attributes
        OnDraw          m_onDraw = nullptr;
        // internal state
        bool            m_valid = false;
        bool            m_dirty = false;
        SDL_Texture*    m_tex = nullptr;
        SDL_Rect        m_rect;
    };
}

#endif // _HJARTA__GFX_LAYER_H_