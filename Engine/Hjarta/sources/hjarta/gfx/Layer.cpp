#include "hjarta/gfx/Layer.h"

#include "hjarta/gfx/Color.h"
#include "Drawer.h"

namespace hjarta
{
    Layer::~Layer()
    {
        destroy();
    }

    //------------------------------------------------------------------------------------------

    void Layer::setup(const OnDraw& _onDraw)
    {
        m_onDraw = _onDraw;
        invalidate();
    }

    //------------------------------------------------------------------------------------------

    void Layer::draw(const SDL_Rect& _rect)
    {
        if (m_rect.w != _rect.w || m_rect.h != m_rect.h)
            invalidate();

        if (!m_valid)
            create(_rect);

        if (m_dirty)
            render();

        HJARTA_RENDERER().copyEx(m_tex, NULL, &_rect, 0, NULL, SDL_FLIP_NONE);
    }

    //------------------------------------------------------------------------------------------

    void Layer::create(const SDL_Rect& _rect)
    {
        destroy();

        if (m_onDraw)
        {
            m_rect = _rect;

            m_tex = SDL_CreateTexture(HJARTA_RENDERER().get(), SDL_PIXELFORMAT_RGBA4444, SDL_TEXTUREACCESS_TARGET, _rect.w, _rect.h);
            SDL_SetTextureBlendMode(m_tex, SDL_BLENDMODE_BLEND);

            m_valid = true;
            m_dirty = true;
        }
    }

    //------------------------------------------------------------------------------------------

    void Layer::destroy()
    {
        if (m_tex)
            SDL_DestroyTexture(m_tex);
    }

    //------------------------------------------------------------------------------------------

    void Layer::render()
    {
        if (m_onDraw)
        {
            HJARTA_RENDERER().pushTarget(m_tex, { m_rect.x, m_rect.y });
            HJARTA_RENDERER().clear(colors::Invisible);
            m_onDraw();
            HJARTA_RENDERER().popColor();
            HJARTA_RENDERER().popTarget();

            m_dirty = false;
        }
    }
}