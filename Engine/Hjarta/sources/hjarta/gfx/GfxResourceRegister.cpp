#include "hjarta/gfx/GfxResourceRegister.h"
#include "hjarta/gfx/Color.h"
#include "hjarta/gfx/Font.h"
#include "hjarta/gfx/Texture.h"
#include "hjarta/gfx/Sprite.h"
#include "hjarta/gfx/Tileset.h"

namespace hjarta
{
    void registerGfxResourceClasses(ResourceManager& _manager)
    {
        _manager.registerResourceClass<Color>();
        _manager.registerResourceClass<FontStyle>();
        _manager.registerResourceClass<Font>();
        _manager.registerResourceClass<Texture>();
        _manager.registerResourceClass<Sprite>();
        _manager.registerResourceClass<Tileset>();
    }
}