#ifndef _HJARTA_GFX_TEXT_LINE_H_
#define _HJARTA_GFX_TEXT_LINE_H_

#include "hjarta/core/Types.h"
#include "hjarta/gfx/Font.h"
#include "hjarta/gfx/Color.h"
#include "hjarta/gfx/DrawContext.h"
#include "hjarta/data/ResourceProxy.h"

#include "SDL.h"

namespace hjarta
{
    // ## TextLine
    // A simple line of text, associated to a font, a font style and a font color
    class TextLine
    {
    public:
        TextLine() = default;
        TextLine(const TextLine &_copy);
        TextLine &operator=(const TextLine &_copy);
        TextLine(TextLine &&_textLine) = default;
        ~TextLine();

    public:
        /// ##### TextLine interface
        // support nullptr
        void                        setFont(Font* _font, bool _refresh = true);
        void                        setFontStyle(const FontStyle* _style, bool _refresh = true);
        void                        setColor(const Color* _color, bool _refresh = true);
        void                        setText(const std::wstring &_text, bool _refresh = true);

        const Font*                 getFont() const { return m_fontProxy.getResource(); }
        const FontStyle*            getFontStyle() const { return m_fontStyleProxy.getResource(); }
        const Color*                getColor() const { return m_colorProxy.getResource(); }
        const std::wstring&         getText() const { return m_text; }
        SizePx                      getSize() const { return m_size; }
        bool                        isValid() const { return m_fontProxy.getResource() != nullptr; }

        void                        draw(DrawContext &_context);
        void                        draw(
            const PointPx &_screenPoint, 
            const PointPx &_anchorPoint = PointPx(0, 0), 
            double _angle = 0, 
            double _zx = 1, double _zy = 1, 
            SDL_RendererFlip _flip = SDL_FLIP_NONE);

    private:
        /// ##### Internal methods
        void                        createTexture();
        void                        releaseTexture();
        void                        refreshTexture();

    private:
        /// ##### Attributes
        ResourceProxy<Font>         m_fontProxy;
        ResourceProxy<FontStyle>    m_fontStyleProxy;
        ResourceProxy<Color>        m_colorProxy;
        std::wstring                m_text;
        // internal state
        SDL_Texture*                m_texture = nullptr;
        SizePx                      m_size;
        DrawContext                 m_context; // internal context for optimization
    };
}

#endif // _HJARTA_GFX_TEXT_LINE_H_