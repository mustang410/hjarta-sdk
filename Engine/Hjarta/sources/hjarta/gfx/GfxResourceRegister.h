#ifndef HJARTA_GfxResourceRegister_h
#define HJARTA_GfxResourceRegister_h

#include "hjarta/data/ResourceManager.h"

namespace hjarta
{
    void registerGfxResourceClasses(ResourceManager& _manager);
}

#endif // HJARTA_GfxResourceRegister_h