#include "hjarta/gfx/TilesetBuilder.h"

namespace hjarta
{
    void TilesetBuilder::serialize(Serializer& _serializer)
    {
        _serializer.serializeContainer("col_prefixes", &m_colPrefixes);
        _serializer.serializeContainer("row_suffixes", &m_rowSuffixes);
        _serializer.serializeObject("init", &m_init);
        _serializer.serializeObject("size", &m_size);

        if (_serializer.isUnserializing())
        {
            buildTiles();
        }
    }

    //------------------------------------------------------------------------------------------

    void TilesetBuilder::buildTiles()
    {
        for (unsigned y = 0; y < unsigned(m_size.h); y++)
        {
            for (unsigned x = 0; x < unsigned(m_size.w); x++)
            {
                std::string key = buildId(x, y);
                SDL_Rect rect = m_init.toSDLRect();
                rect.x = m_init.point.x + (m_init.size.w + 1) * x;
                rect.y = m_init.point.y + (m_init.size.h + 1) * y;
                m_tiles.emplace_back(key, rect);
            }
        }
    }

    //------------------------------------------------------------------------------------------

    std::string TilesetBuilder::buildId(int _x, int _y)
    {
        std::string prefix;
        if (!m_colPrefixes.empty())
        {
            if (m_size.w <= int(m_colPrefixes.size()))
            {
                prefix = m_colPrefixes[_x];
            }
            else
            {
                prefix = m_colPrefixes[_x % m_colPrefixes.size()] + std::to_string(_x);
            }
        }
        else if (m_size.w > 1)
        {

            prefix = std::to_string(_x);
        }

        std::string suffix;
        if (!m_rowSuffixes.empty())
        {
            if (m_size.h <= int(m_rowSuffixes.size()))
            {
                suffix = m_rowSuffixes[_y];
            }
            else
            {
                suffix = m_rowSuffixes[_y % m_rowSuffixes.size()] + std::to_string(_y);
            }
        }
        else if (m_size.h > 1)
        {
            suffix = std::to_string(_y);
        }

        if (!prefix.empty() && !suffix.empty())
            return prefix + "_" + suffix;
        else if (!prefix.empty() && suffix.empty())
            return prefix;
        else if (prefix.empty() && !suffix.empty())
            return suffix;
        else
            return "default";
    }
}