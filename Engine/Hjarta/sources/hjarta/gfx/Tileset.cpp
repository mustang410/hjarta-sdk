#include "hjarta/gfx/Tileset.h"

#include "hjarta/tools/Log.h"

namespace hjarta
{
    void Tileset::serialize(Serializer& _serializer)
    {
        _serializer.serializeObject("texture_proxy", &m_textureProxy);

        // assign texture to sprites
        SerialHookAutoRegister<Sprite> prehook(&_serializer, ISerialHook::PreProcess, [&](Sprite* _sprite)
        {
            if (_serializer.isUnserializing())
            {
                _sprite->setTexture(m_textureProxy.getResource());
            }
        });

        _serializer.serializeMap("sprites", &m_sprites, Serializer::Optional);
        _serializer.serializeMap("builders", &m_tilesets, Serializer::Optional);

        if (_serializer.isUnserializing())
        {
            for (auto &it : m_tilesets)
            {
                for (const TilesetBuilder::Tile &tile : it.second.getTiles())
                {
                    m_sprites.emplace(std::make_pair(tile.key, Sprite(tile.rect)));
                }
            }

            bindTexture();
        }
    }

    //------------------------------------------------------------------------------------------

    Tileset::~Tileset()
    {
        if (!m_missings.empty())
        {
            HJARTA_LOG_ERROR("Some sprites were missing for tileset <", getSerialURL().getNormalizedString(), ">:");
            for (const std::string& missing : m_missings)
            {
                HJARTA_LOG_ERROR("- ", missing);
            }
        }
    }

    //------------------------------------------------------------------------------------------

    std::vector<std::string> Tileset::verify(const std::vector<std::string>& _ids) const
    {
        std::vector<std::string> missings;
        for (const std::string& id : _ids)
        {
            if (m_sprites.find(id) == m_sprites.end())
                missings.push_back(id);
        }
        return missings;
    }

    //------------------------------------------------------------------------------------------

    Sprite* Tileset::getSprite(const std::string& _id)
    {
        auto it = m_sprites.find(_id);
        if (it != m_sprites.end())
            return &it->second;

        if (m_missings.find(_id) == m_missings.end())
        {
            HJARTA_LOG_ERROR("Sprite <", _id, "> is missing from Tileset <", getSerialURL().getNormalizedString(), ">");
            m_missings.emplace(_id);
        }

        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    const Sprite* Tileset::getSprite(const std::string& _id) const
    {
        auto it = m_sprites.find(_id);
        if (it != m_sprites.end())
            return &it->second;

        if (m_missings.find(_id) == m_missings.end())
        {
            HJARTA_LOG_ERROR("Sprite <", _id, "> is missing from Tileset <", getSerialURL().getNormalizedString(), ">");
            m_missings.emplace(_id);
        }

        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    void Tileset::bindTexture()
    {
        for (auto& it : m_sprites)
        {
            it.second.setTexture(m_textureProxy.getResource());
        }
    }
}