#ifndef _HJARTA_GFX_DRAWER_H_
#define _HJARTA_GFX_DRAWER_H_

#include "hjarta/core/Types.h"
#include "hjarta/gfx/Renderer.h"
#include "hjarta/serialization/Serializer.h"
#include "hjarta/gfx/Color.h"

#include "SDL.h"
#include "SDL_image.h"

namespace hjarta
{
    // ## Drawer
    // Provides method to draw simple forms
    class Drawer
    {
    public:
        /// ##### Drawing methods
        static void drawLine(const Point2D<int> &_a, const Point2D<int> &_b, const Color &_color);
        static void drawRect(const Rect2D<int, int> &_rect, const Color &_color);
        static void drawCross(const Rect2D<int, int> &_rect, const Color &_color);
        static void drawCross(const Point2D<int> &_point, const int _radius, const Color &_color);
        static void fillRect(const Rect2D<int, int> &_rect, const Color &_color);
        static void fillZebra(const Rect2D<int, int> &_rect, const Color &_color);
    };
}

#endif // _HJARTA_GFX_DRAWER_H_