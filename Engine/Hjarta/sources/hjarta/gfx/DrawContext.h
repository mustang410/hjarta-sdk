#ifndef HJARTA_DrawContext_h
#define HJARTA_DrawContext_h

#include "hjarta/core/Types.h"
#include "hjarta/gfx/Renderer.h"
#include "hjarta/core/Application.h"

#include "SDL.h"

namespace hjarta
{
    // ## DrawContext
    // Context used to optimize drawing variables computing
    // Values are computed only when required
    class DrawContext
    {
    public:
        DrawContext() = default;
        DrawContext(
            const PointPx &_screenPoint,
            const PointPx &_anchorPoint = PointPx(0, 0),
            double _angle = 0, 
            double _zx = 1, double _zy = 1,
            SDL_RendererFlip _flip = SDL_FLIP_NONE);
        ~DrawContext() = default;

    public:
        /// ##### DrawContext interface
        inline void             invalidate() { m_valid = false; }
        void                    set(
            const PointPx &_screenPoint,                    // position on the screen in pixels
            const PointPx &_anchorPoint = PointPx(0, 0),    // anchor point on the sprite in pixels
            double _angle = 0,                              // rotation angle in degrees
            double _zx = 1, double _zy = 1,                 // x and y factor
            SDL_RendererFlip _flip = SDL_FLIP_NONE);        // flip flag
        void                    setFactor(double _zx, double _zy);
        void                    compute(const SDL_Rect &_srcRect, double _magnification);

        /// ##### Accessor
        inline SDL_Rect*        getSrcRect() { return &m_srcRect; }
        inline SDL_Rect*        getDstRect() { return &m_dstRect; }
        inline double           getAngle() { return m_angle; }
        inline SDL_Point*       getCenter() { return &m_center; }
        inline SDL_RendererFlip getFlip() { return m_flip; }
        inline bool             requireRenderEx() const { return true; }// m_renderEx;

    private:
        /// ##### Attributes
        bool                    m_valid = false;
        PointPx                 m_screenPoint;
        PointPx                 m_anchorPoint;
        PointPx                 m_rendererOffset;
        SDL_Rect                m_srcRect;
        SDL_Rect                m_dstRect;
        double                  m_angle = 0;
        double                  m_zx = 1;
        double                  m_zy = 1;
        SDL_Point               m_center;
        SDL_RendererFlip        m_flip = SDL_FLIP_NONE;
        bool                    m_renderEx = false;
       
        double                  m_magnification;
    };
}

#endif // HJARTA_DrawContext_h