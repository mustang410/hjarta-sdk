#ifndef HJARTA_Font_h
#define HJARTA_Font_h

#include "hjarta/core/Types.h"
#include "hjarta/data/Resource.h"

#include "SDL.h"
#include "SDL_ttf.h"

namespace hjarta
{
    using FontSize = unsigned;

    // ## FontStyle
    // Defines a set of properties for a font
    class FontStyle : public Resource
    {
        HJARTA_DECL_RESOURCE_CLONABLE(hjarta::FontStyle, hjarta::Resource)

    public:
        /// ##### Serialization
        void                            serialize(Serializer &_serializer);

    public:
        /// ##### Custom types
        enum Style { Regular, Bold, Italic, Underlined, Striped };

        /// ##### Constants
        static const FontSize DefaultSize;

    public:
        FontStyle() { setStyle(Style::Regular); }
        FontStyle(FontSize _size, Style _style = Regular) : m_size(_size) { setStyle(_style); }
        ~FontStyle() = default;

        /// ##### Mapping
        bool operator<(const FontStyle &_style) const { return (m_size == _style.m_size ? m_style < _style.m_style : m_size < _style.m_size); }

    public:
        /// ##### FontStyle interface
        // size
        void                            setSize(FontSize _size) { m_size = _size; }
        FontSize                        getSize() const { return m_size; }
        // style
        void                            setStyle(Style _style) { m_style = enumStyleToFlag(_style); }
        void                            addStyle(Style _style) { m_style |= enumStyleToFlag(_style); }
        void                            removeStyle(Style _style) { m_style &= ~enumStyleToFlag(_style); }
        int                             getStyle() const { return m_style; }

    private:
        /// ##### Helpers
        int                             enumStyleToFlag(Style _style);

    private:
        /// ##### Attributes
        FontSize                        m_size = DefaultSize;
        int                             m_style;
    };

    // ## Font
    // Handle a TTF font
    // A font handles many SDL font, associated to sizes
    // Create a default font in case of error
    class Font : public Resource
    {
        HJARTA_DECL_RESOURCE_NON_CLONABLE(hjarta::Font, hjarta::Resource)

    public:
        /// ##### Serialization
        void                            serialize(Serializer &_serializer) override;

    public:
        /// ##### Custom types
        static const Path DefaultPath;

    public:
        Font() = default;
        Font(const Path &_path);
        ~Font();

    public:
        void                            unload(); // release font for all loaded sizes

        /// ##### Getters
        TTF_Font*                       get(FontStyle _style);
        const Path&                     getPath() const { return m_path; }
        bool                            isValid() const { return !m_errorOccurred; }

    private:
        /// ##### Internal methods
        bool                            find(FontStyle _style) { return (m_fonts.find(_style) != m_fonts.end()); }
        bool                            load(const Path &_path, FontStyle _style);
        void                            create(FontStyle _style);

    private:
        /// ##### Attributes
        bool                            m_errorOccurred = false;
        Path                            m_path;
        std::map<FontStyle, TTF_Font*>  m_fonts;
    };
}

#endif // HJARTA_Font_h