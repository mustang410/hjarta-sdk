#include "hjarta/gfx/Drawer.h"

#include "hjarta/core/Application.h"

namespace hjarta
{
    void Drawer::drawLine(const Point2D<int> &_a, const Point2D<int> &_b, const Color &_color)
    {
        HJARTA_RENDERER().pushColor(_color);
        HJARTA_RENDERER().drawLine(_a, _b);
        HJARTA_RENDERER().popColor();
    }

    void Drawer::drawRect(const Rect2D<int, int> &_rect, const Color &_color)
    {
        Point2D<int> a = _rect.point;
        Point2D<int> b = Point2D<int>(a.x + _rect.size.w, a.y);
        Point2D<int> c = Point2D<int>(b.x, b.y + _rect.size.h);
        Point2D<int> d = Point2D<int>(a.x, a.y + _rect.size.h);
        drawLine(a, b, _color);
        drawLine(b, c, _color);
        drawLine(c, d, _color);
        drawLine(d, a, _color);
    }

    void Drawer::drawCross(const Rect2D<int, int> &_rect, const Color &_color)
    {
        Point2D<int> a(_rect.point.x + _rect.size.w / 2, _rect.point.y);
        Point2D<int> b(_rect.point.x + _rect.size.w / 2, _rect.point.y + _rect.size.h);
        Point2D<int> c(_rect.point.x, _rect.point.y + _rect.size.h / 2);
        Point2D<int> d(_rect.point.x + _rect.size.w, _rect.point.y + _rect.size.h / 2);
        drawLine(a, b, _color);
        drawLine(c, d, _color);
    }

    void Drawer::drawCross(const Point2D<int> &_point, const int _radius, const Color &_color)
    {
        Rect2D<int, int> rect(_point.x - _radius, _point.y - _radius, _radius * 2, _radius * 2);
        drawCross(rect, _color);
    }

    void Drawer::fillRect(const Rect2D<int, int> &_rect, const Color &_color)
    {
        HJARTA_RENDERER().pushColor(_color);
        HJARTA_RENDERER().fillRect(_rect);
        HJARTA_RENDERER().popColor();
    }

    void Drawer::fillZebra(const Rect2D<int, int> &_rect, const Color &_color)
    {
        const double Angle = M_PI / 2;
        const int Space = 8;
        for (int x = 0; x < _rect.size.w; x += Space)
        {
            double dy = _rect.size.h;
            double dx = std::sin(Angle) * dy;
            if (x + dx >= _rect.size.w)
            {
                dx = _rect.size.w - x;
                dy = std::sin(Angle) * dx;

                if (dy >= _rect.size.h)
                {
                    dy = _rect.size.h;
                    dx = std::sin(Angle) * dy;
                }
            }
            Point2D<int> a(_rect.point.x + x, _rect.point.y);
            Point2D<int> b(a.x + int(dx), a.y + int(dy));
            drawLine(a, b, _color);
        }
        for (int y = 0; y < _rect.size.h; y += Space)
        {
            double dy = _rect.size.h;
            double dx = std::sin(Angle) * dy;
            if (y + dy >= _rect.size.h)
            {
                dy = _rect.size.h - y;
                dx = std::sin(Angle) * dy;

                if (dx >= _rect.size.w)
                {
                    dx = _rect.size.w;
                    dy = std::sin(Angle) * dx;
                }
            }
            Point2D<int> a(_rect.point.x, _rect.point.y + y);
            Point2D<int> b(a.x + int(dx), a.y + int(dy));
            drawLine(a, b, _color);
        }
    }
}