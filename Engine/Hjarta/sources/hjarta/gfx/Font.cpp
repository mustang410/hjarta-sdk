#include "hjarta\gfx\Font.h"

#include "hjarta/tools/Log.h"
#include "hjarta\core\Application.h"

namespace hjarta
{
    const FontSize FontStyle::DefaultSize = 10;

    //------------------------------------------------------------------------------------------

    void FontStyle::serialize(Serializer &_serializer)
    {
        _serializer.serializeObject("size", &m_size);
        // style
        bool bold = false, italic = false, underline = false, striped = false;
        _serializer.serializeObject("bold", &bold, Serializer::Optional);
        _serializer.serializeObject("italic", &italic, Serializer::Optional);
        _serializer.serializeObject("underlined", &underline, Serializer::Optional);
        _serializer.serializeObject("striped", &striped, Serializer::Optional);
        if (bold)
            addStyle(Bold);
        if (italic)
            addStyle(Italic);
        if (underline)
            addStyle(Underlined);
        if (striped)
            addStyle(Striped);
    }

    //------------------------------------------------------------------------------------------

    int FontStyle::enumStyleToFlag(Style _style)
    {
        switch (_style)
        {
        case FontStyle::Regular:
            return TTF_STYLE_NORMAL;
        case FontStyle::Bold:
            return TTF_STYLE_BOLD;
        case FontStyle::Italic:
            return TTF_STYLE_ITALIC;
        case FontStyle::Underlined:
            return TTF_STYLE_UNDERLINE;
        case FontStyle::Striped:
            return TTF_STYLE_STRIKETHROUGH;
        }
        return TTF_STYLE_NORMAL;
    }

    //------------------------------------------------------------------------------------------

    const Path Font::DefaultPath = "data/gfx/default_font.ttf";

    //------------------------------------------------------------------------------------------

    void Font::serialize(Serializer &_serializer)
    {
        _serializer.serializeObject("path", &m_path);
    }

    //------------------------------------------------------------------------------------------

    Font::Font(const Path &_path) :
        m_path(_path),
        m_errorOccurred(false)
    {

    }

    //------------------------------------------------------------------------------------------

    Font::~Font()
    {
        unload();
    }

    //------------------------------------------------------------------------------------------

    void Font::unload()
    {
        // closes all opened fonts
        for (auto &font : m_fonts)
        {
            TTF_CloseFont(font.second);
        }
    }

    //------------------------------------------------------------------------------------------

    TTF_Font *Font::get(FontStyle _style)
    {
        // font creation
        if (!find(_style))
            create(_style);

        return m_fonts[_style];
    }

    //------------------------------------------------------------------------------------------

    bool Font::load(const Path &_path, FontStyle _style)
    {
        TTF_Font *buffer = TTF_OpenFont(_path.data(), _style.getSize());

        // unable to load the font
        if (buffer == nullptr) 
            return false;
        TTF_SetFontStyle(buffer, _style.getStyle());

        // save the font
        m_fonts[_style] = buffer;
        return true;
    }

    //------------------------------------------------------------------------------------------

    void Font::create(FontStyle _style)
    {
        // try to load the font
        if (!load(m_path, _style))
        {
            // error occurred
            HJARTA_LOG_ERROR("Unable to load the font <", m_path, ">");
            m_errorOccurred = true;

            // try to load the default texture
            if (!load(DefaultPath, _style))
            {
                // fatal error occurred
                HJARTA_LOG_ERROR("Unable to load the default font <", DefaultPath, ">");
                HJARTA_LOG_ERROR("Data folder may be corrupted");
                HJARTA_FATAL_ERROR();
            }
        }
    }
}