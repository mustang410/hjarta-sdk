#include "hjarta/gfx/DrawContext.h"

namespace hjarta
{
    DrawContext::DrawContext(const PointPx &_screenPoint, const PointPx &_anchorPoint, double _angle, double _zx, double _zy, SDL_RendererFlip _flip) :
        m_valid(false)
    {
        set(_screenPoint, _anchorPoint, _angle, _zx, _zy, _flip);
    }

    //------------------------------------------------------------------------------------------

    void DrawContext::set(const PointPx &_screenPoint, const PointPx &_anchorPoint, double _angle, double _zx, double _zy, SDL_RendererFlip _flip)
    {
        if (_screenPoint != m_screenPoint || _anchorPoint != m_anchorPoint
            || _angle != m_angle
            || _zx != m_zx || _zy != m_zy
            || _flip != m_flip)
        {
            m_screenPoint = _screenPoint;
            m_anchorPoint = _anchorPoint;
            m_angle = _angle;
            m_zx = _zx;
            m_zy = _zy;
            m_flip = _flip;

            m_renderEx = (m_angle != 0 || m_zx != 1 || m_zy != 1 || m_flip != SDL_RendererFlip::SDL_FLIP_NONE);

            invalidate();
        }
    }

    //------------------------------------------------------------------------------------------

    void DrawContext::setFactor(double _zx, double _zy)
    {
        if (m_zx != _zx && m_zy != _zy)
        {
            m_zx = _zx;
            m_zy = _zy;

            invalidate();
        }
    }

    //------------------------------------------------------------------------------------------

    void DrawContext::compute(const SDL_Rect &_srcRect, double _magnification)
    {
        if (!m_valid
            || m_srcRect.w != _srcRect.w || m_srcRect.h != _srcRect.h || m_srcRect.x != _srcRect.x || m_srcRect.y != _srcRect.y
            || m_magnification != _magnification
            || m_rendererOffset != HJARTA_RENDERER().getOffset())
        {
            m_srcRect = _srcRect;
            m_magnification = _magnification;

            // process zoom and positionning
            m_center.x = (int)(m_anchorPoint.x * m_zx);
            m_center.y = (int)(m_anchorPoint.y * m_zy);
            m_dstRect.x = m_screenPoint.x - m_center.x;
            m_dstRect.y = m_screenPoint.y - m_center.y;
            m_dstRect.w = (int)(m_srcRect.w * m_magnification * m_zx);
            m_dstRect.h = (int)(m_srcRect.h * m_magnification * m_zy);

            // process offset
            m_rendererOffset = HJARTA_RENDERER().getOffset();
            if (m_rendererOffset.x != 0 || m_rendererOffset.y != 0)
            {
                m_dstRect.x -= m_rendererOffset.x;
                m_dstRect.y -= m_rendererOffset.y;
            }

            m_valid = true;
        }
    }
}