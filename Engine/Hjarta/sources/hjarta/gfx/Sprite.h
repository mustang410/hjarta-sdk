#ifndef HJARTA_Sprite_h
#define HJARTA_Sprite_h

#include "hjarta\core\Types.h"
#include "hjarta\core\Enum.h"
#include "hjarta\data\Resource.h"
#include "hjarta\data\ResourceProxy.h"
#include "hjarta\gfx\Texture.h"
#include "hjarta\gfx\DrawContext.h"

#include <map>

namespace hjarta
{
    // ## Sprite
    // A sprite is referenced to a texture and is used to draw a part of it
    // A sprite could either be animated or not
    class Sprite : public Resource
    {
        HJARTA_DECL_RESOURCE_CLONABLE(hjarta::Sprite, hjarta::Resource)

    public:
        // ### Running mode
        // Availables mode to run an animation
        enum class RunningMode { ToRight, ToLeft, Count };

    public:
        /// ##### Serialization
        void                    serialize(Serializer& _serializer) override;

    public:
        /// ##### Constructors&  destructor
        Sprite() = default;
        Sprite(const SDL_Rect& _rect);
        Sprite(const Sprite& _copy) = default;
        Sprite(Texture* _tex, const SDL_Rect& _rect);
        ~Sprite() = default;

        /// ##### Getters & setters
        SDL_Rect                getRect() const;
        SizePx                  getSize() const;
        bool                    isAnimated() const { return m_animated; }
        bool                    isValid() const { return m_rect.size.w > 0 && m_rect.size.h > 0; }

        void                    setSprite(Texture* _tex, const SDL_Rect& _rect);
        void                    setTexture(Texture* _tex);
        void                    setAnimation(unsigned _frameNb, const std::vector<double>& _delay, bool _looping, bool _run = true);
        void                    setAnimRun(bool _run);
        void                    setAnimRunningMode(RunningMode _runnningMode);
        void                    setAnimCurrentFrame(unsigned _currentFrame, bool _run);
        void                    toggleAnimRunningMode();

    public:
        /// ##### Sprite interface
        void                    draw(DrawContext& _context);
        void                    draw(
            const PointPx& _screenPoint, 
            const PointPx& _anchorPoint = PointPx(0, 0), 
            double _angle = 0, 
            double _zx = 1, double _zy = 1, 
            SDL_RendererFlip _flip = SDL_FLIP_NONE);

    private:
        /// ##### Internal methods
        bool                    animate();

    private:
        /// ##### Attributes
        ResourceProxy<Texture>  m_textureProxy;
        RectPx                  m_rect;
        // animation handling
        bool m_animated = false;
        struct Animation : public Serializable
        {
            /// ##### Serialization
            void                serialize(Serializer& _serializer);

            /// ##### Helpers
            void                computeRect(RectPx& _src) { _src.point.x = m_currentFrame * (_src.size.w + 1); }

            /// ##### Attributes
            bool                m_run = true;
            RunningMode         m_runningMode = RunningMode::ToRight;
            unsigned            m_frameCount = 1;       // number of frames for this animation
            std::vector<double> m_delays = { 100.f };   // array of delay between frames
            bool                m_looping = true;       // flag for auto looping
            // internal state
            double              m_timer = 0;
            unsigned            m_currentFrame = 0;
        };
        Animation               m_animation;
        DrawContext             m_context; // internal context for optimization
    };
}

HJARTA_DECL_ENUM_HELPER(hjarta::Sprite::RunningMode, {
    { hjarta::Sprite::RunningMode::ToRight, "to_right" },
    { hjarta::Sprite::RunningMode::ToLeft, "to_left" }
    });

#endif // HJARTA_Sprite_h