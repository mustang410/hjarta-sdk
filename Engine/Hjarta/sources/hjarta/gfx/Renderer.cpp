#include "hjarta/gfx/Renderer.h"

#include "hjarta/tools/Log.h"

namespace hjarta
{
    Renderer::~Renderer()
    {
        destroy();
    }

    //------------------------------------------------------------------------------------------

    bool Renderer::init(SDL_Window *_window)
    {
        HJARTA_ASSERT(!m_ren, "Renderer already initialized");
        m_ren = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
        if (m_ren == nullptr)
        {
            HJARTA_LOG_INFO("Error occured during the renderer creation");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
            return false;
        }
        if (SDL_SetRenderDrawBlendMode(m_ren, SDL_BLENDMODE_BLEND) != 0)
        {
            HJARTA_LOG_INFO("Error occured during the renderer blend mode setting");
            HJARTA_LOG_INFO("SDL error: ", SDL_GetError());
            return false;
        }
        return true;
    }

    //------------------------------------------------------------------------------------------

    void Renderer::destroy()
    {
        HJARTA_ASSERT(m_ren, "Renderer not initialized");
        SDL_DestroyRenderer(m_ren);
        m_ren = nullptr;
    }

    //------------------------------------------------------------------------------------------

    void Renderer::clear(const Color &_color)
    {
        if (m_targets.empty())
        {
            m_copyPerFrame = m_copyPerFrameCounter;
            m_copyPerFrameCounter = 0;
            m_drawPerFrame = m_drawPerFrameCounter;
            m_drawPerFrameCounter = 0;
        }
            

        pushColor(_color);
        SDL_RenderClear(m_ren);
    }

    //------------------------------------------------------------------------------------------

    const PointPx &Renderer::getOffset() const
    {
        static PointPx Origin(0, 0);
        return (m_targets.empty() ? Origin : m_targets.back().getOffset());
    }

    //------------------------------------------------------------------------------------------

    void Renderer::copy(SDL_Texture *_tex, const SDL_Rect *_src, const SDL_Rect *_dst)
    {
        m_copyPerFrameCounter++;
        SDL_RenderCopy(m_ren, _tex, _src, _dst);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::copyEx(SDL_Texture *_tex, const SDL_Rect *_src, const SDL_Rect *_dst, const double _angle, const SDL_Point *_center, const SDL_RendererFlip _flip)
    {
        m_copyPerFrameCounter++;
        SDL_RenderCopyEx(m_ren, _tex, _src, _dst, _angle, _center, _flip);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::drawLine(const PointPx& _a, const PointPx& _b)
    {
        m_drawPerFrameCounter++;
        PointPx offset = getOffset();
        SDL_RenderDrawLine(m_ren, _a.x - offset.x, _a.y - offset.y, _b.x - offset.x, _b.y - offset.y);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::fillRect(const RectPx& _rect)
    {
        m_drawPerFrameCounter++;
        PointPx offset = getOffset();
        SDL_Rect rect = { _rect.point.x - offset.x, _rect.point.y - offset.y, _rect.size.w, _rect.size.h };
        SDL_RenderFillRect(m_ren, &rect);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::present()
    {
        popColor();
        SDL_RenderPresent(m_ren);

        HJARTA_ASSERT(m_cliprects.empty(), "Non symetric call to push/pop clip rect");
        HJARTA_ASSERT(m_colors.empty(), "Non symetric call to push/pop color");
    }

    //------------------------------------------------------------------------------------------

    void Renderer::pushColor(const Color &_color)
    {
        bool found = false;
        if (!m_colors.empty())
        {
            if (m_colors.back().getColor() == _color)
            {
                m_colors.back().increment();
                found = true;
            } 
        }
        
        if (!found)
        {
            m_colors.emplace_back(_color);
            SDL_SetRenderDrawColor(m_ren, _color.r, _color.g, _color.b, _color.a);
        }
        
    }

    //------------------------------------------------------------------------------------------

    void Renderer::popColor()
    {
        if (!m_colors.empty())
        {
            m_colors.back().decrement();
            if (!m_colors.back().isReferenced())
            {
                m_colors.pop_back();
            }
        }

        if (!m_colors.empty())
        {
            const Color &color = m_colors.back().getColor();
            SDL_SetRenderDrawColor(m_ren, color.r, color.g, color.b, color.a);
        }
        else
        {
            SDL_SetRenderDrawColor(m_ren, 0, 0, 0, 0);
        }
    }

    //------------------------------------------------------------------------------------------

    void Renderer::pushClipRect(const RectPx&_cliprect)
    {
        RectPx shifted = _cliprect;
        shifted.point.x -= getOffset().x;
        shifted.point.y -= getOffset().y;
        SDL_RenderSetClipRect(m_ren, &shifted.toSDLRect());
        m_cliprects.push_back(shifted);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::popClipRect()
    {
        if (!m_cliprects.empty())
            m_cliprects.pop_back();

        if (!m_cliprects.empty())
        {
            RectPx shifted = m_cliprects.back();
            SDL_RenderSetClipRect(m_ren, &shifted.toSDLRect());
        }
        else
        {
            SDL_RenderSetClipRect(m_ren, nullptr);
        }
    }

    //------------------------------------------------------------------------------------------

    void Renderer::pushTarget(SDL_Texture *_target, const PointPx &_offset)
    {
        m_targets.emplace_back(_target, _offset);
        SDL_SetRenderTarget(m_ren, _target);
    }

    //------------------------------------------------------------------------------------------

    void Renderer::popTarget()
    {
        m_targets.pop_back();
        if (m_targets.empty())
        {
            SDL_SetRenderTarget(m_ren, nullptr);
        }
        else
        {
            SDL_SetRenderTarget(m_ren, m_targets.back().getTarget());
        }
    }
}