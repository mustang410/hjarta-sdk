#include "hjarta/gfx/TextLine.h"

#include "hjarta/core/Application.h"
#include "hjarta/tools/Log.h"

namespace hjarta
{
    TextLine::TextLine(const TextLine &_copy) :
        m_fontProxy(_copy.m_fontProxy),
        m_fontStyleProxy(_copy.m_fontStyleProxy),
        m_colorProxy(_copy.m_colorProxy),
        m_text(_copy.m_text)
    {
        createTexture();
    }

    //------------------------------------------------------------------------------------------

    TextLine &TextLine::operator=(const TextLine &_copy)
    {
        m_fontProxy = _copy.m_fontProxy;
        m_fontStyleProxy = _copy.m_fontStyleProxy;
        m_colorProxy = _copy.m_colorProxy;
        m_text = _copy.m_text;
        createTexture();
        return *this;
    }

    //------------------------------------------------------------------------------------------

    TextLine::~TextLine()
    {
        releaseTexture();
    }

    //------------------------------------------------------------------------------------------

    void TextLine::setFont(Font* _font, bool _refresh)
    {
        if (_font)
        {
            m_fontProxy.shareResource(_font);
            if (_refresh)
                refreshTexture();
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::setFontStyle(const FontStyle* _style, bool _refresh)
    {
        if (_style)
        {
            m_fontStyleProxy.cloneResource(*_style);
            if (_refresh)
                refreshTexture();
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::setColor(const Color* _color, bool _refresh)
    {
        if (_color)
        {
            m_colorProxy.cloneResource(*_color);
            if (_refresh)
                refreshTexture();
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::setText(const std::wstring& _text, bool _refresh)
    {
        m_text = _text;
        if (_refresh)
            refreshTexture();
    }

    //------------------------------------------------------------------------------------------

    void TextLine::draw(DrawContext &_context)
    {
        if (m_texture == nullptr)
            createTexture();

        _context.compute({0, 0, m_size.w, m_size.h}, 1);

        if (m_texture)
        {
            if (_context.requireRenderEx())
                HJARTA_RENDERER().copyEx(m_texture, 
                    NULL, _context.getDstRect(), 
                    _context.getAngle(), _context.getCenter(), _context.getFlip());
            else
                HJARTA_RENDERER().copy(m_texture, 
                    _context.getSrcRect(), _context.getDstRect());
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::draw(const PointPx &_screenPoint, const PointPx &_anchorPoint, double _angle, double _zx, double _zy, SDL_RendererFlip _flip)
    {
        m_context.set(_screenPoint, _anchorPoint, _angle, _zx, _zy, _flip);
        draw(m_context);
    }

    //------------------------------------------------------------------------------------------

    void TextLine::createTexture()
    {
        HJARTA_ASSERT((m_texture == nullptr), "Texture has to be released first");
        if (m_fontProxy.hasResource())
        {
            // converting text
            size_t wlen = m_text.length();
            Uint16 *str;

            if (wlen > 0)
            {
                str = new Uint16[wlen + 1];;
                for (size_t i = 0; i < wlen; i++) {
                    str[i] = m_text[i];
                }
                str[wlen] = '\0';
            }
            else
            {
                str = new Uint16[2];
                str[0] = ' ';
                str[1] = '\0';
            }

            // creating texture
            SDL_Surface *buffer = TTF_RenderUNICODE_Blended(m_fontProxy->get(m_fontStyleProxy.getSafeResource("Missing font style")), str, m_colorProxy.getSafeResource("Missing color proxy"));
            if (m_texture != nullptr) 
                SDL_DestroyTexture(m_texture); // release previous texture
            m_texture = SDL_CreateTextureFromSurface(HJARTA_RENDERER().get(), buffer);
            SDL_QueryTexture(m_texture, NULL, NULL, &m_size.w, &m_size.h);

            // release memory
            delete[] str;
            SDL_FreeSurface(buffer);
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::releaseTexture()
    {
        if (m_texture != nullptr)
        {
            SDL_DestroyTexture(m_texture);
            m_texture = nullptr;
        }
    }

    //------------------------------------------------------------------------------------------

    void TextLine::refreshTexture()
    {
        releaseTexture();
        createTexture();
    }
}