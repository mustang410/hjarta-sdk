#include "hjarta\gfx\Sprite.h"
#include "hjarta\core\Application.h"

namespace hjarta
{
    void Sprite::serialize(Serializer& _serializer)
    {
        Serializer::ParameterType textureMandatory = Serializer::Mandatory;
        if (_serializer.isUnserializing() && m_textureProxy.hasResource())
            textureMandatory = Serializer::Optional;
        _serializer.serializeObject("texture_proxy", &m_textureProxy, textureMandatory);
        _serializer.serializeObject("rect", &m_rect);
        _serializer.serializeObject("animated", &m_animated, Serializer::Optional);
        if (m_animated)
        {
            _serializer.serializeObject("animation", &m_animation);
        }
    }

    //------------------------------------------------------------------------------------------

    void Sprite::Animation::serialize(Serializer& _serializer)
    {
        _serializer.serializeObject("run", &m_run);
        _serializer.serializeEnum("mode", &m_runningMode);
        _serializer.serializeObject("frame_nb", &m_frameCount);
        _serializer.serializeContainer("delays", &m_delays);
        _serializer.serializeObject("looping", &m_looping);
    }

    //------------------------------------------------------------------------------------------

    Sprite::Sprite(const SDL_Rect& _rect)
    {
        m_rect = _rect;
        m_animated = false;
    }

    //------------------------------------------------------------------------------------------

    Sprite::Sprite(Texture* _tex, const SDL_Rect& _rect)
    {
        setSprite(_tex, _rect);
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setSprite(Texture* _tex, const SDL_Rect& _rect)
    {
        setTexture(_tex);
        m_rect = _rect;
        // no animation
        m_animated = false;
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setTexture(Texture* _tex)
    {
        m_textureProxy.shareResource(_tex);
    }

    //------------------------------------------------------------------------------------------

    SDL_Rect Sprite::getRect() const
    {
        if (const Texture* texture = m_textureProxy.getResource())
        {
            double magn = texture->getMagnification();
            if (magn != 1)
                return m_rect.rescale(0, 0, magn, magn).toSDLRect();
        }

         return m_rect.toSDLRect(); 
    }

    //------------------------------------------------------------------------------------------

    SizePx Sprite::getSize() const
    { 
        if (const Texture* texture = m_textureProxy.getResource())
        {
            double magn = texture->getMagnification();
            if (magn != 1)
                return m_rect.rescale(0, 0, magn, magn).size;
        }

         return m_rect.size;
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setAnimation(unsigned _frameNb, const std::vector<double>& _delay, bool _looping, bool _run)
    {
        m_animation.m_run = _run;
        m_animation.m_runningMode = RunningMode::ToRight;
        m_animation.m_currentFrame = 0;
        m_animation.m_frameCount = _frameNb;
        m_animation.m_delays = _delay;
        m_animation.m_timer = SDL_GetTicks();
        m_animation.m_looping = _looping;
        m_animated = true;

        // delay checking
        if (m_animation.m_delays.size() < _frameNb)
        {
            size_t delaySize = m_animation.m_delays.size();
            for (size_t i = 0; i <= _frameNb - delaySize; i++)
            {
                if (m_animation.m_delays.size() > 0) m_animation.m_delays.push_back(m_animation.m_delays.back());
                else m_animation.m_delays.push_back(1000); // default value
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setAnimRun(bool _run)
    {
        if (m_animated) m_animation.m_run = _run;
        m_animation.m_timer = SDL_GetTicks();
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setAnimRunningMode(RunningMode _runnningMode)
    {
        if (m_animated) m_animation.m_runningMode = _runnningMode;
    }

    //------------------------------------------------------------------------------------------

    void Sprite::toggleAnimRunningMode()
    {
        if (m_animated)
        {
            if (m_animation.m_runningMode == RunningMode::ToLeft) m_animation.m_runningMode = RunningMode::ToRight;
            else m_animation.m_runningMode = RunningMode::ToLeft;
        }
    }

    //------------------------------------------------------------------------------------------

    void Sprite::setAnimCurrentFrame(unsigned _currentFrame, bool _run)
    {
        if (m_animated)
        {
            if (_currentFrame < m_animation.m_frameCount) m_animation.m_currentFrame = _currentFrame;
            else m_animation.m_currentFrame = m_animation.m_frameCount - 1;
            m_animation.m_run = _run;
        }
    }

    //------------------------------------------------------------------------------------------

    void Sprite::draw(DrawContext& _context)
    {
        if (Texture* texture = m_textureProxy.getResource())
        {
            animate();
            RectPx rect = m_rect;
            if (m_animated)
                rect.point.x = m_animation.m_currentFrame * (rect.size.w + 1);
            _context.compute(rect.toSDLRect(), texture->getMagnification());

            // render
            if (_context.requireRenderEx())
                HJARTA_RENDERER().copyEx(texture->get(),
                    _context.getSrcRect(), _context.getDstRect(),
                    _context.getAngle(), _context.getCenter(), _context.getFlip());
            else
                HJARTA_RENDERER().copy(texture->get(),
                    _context.getSrcRect(), _context.getDstRect());
        }
    }

    //------------------------------------------------------------------------------------------

    void Sprite::draw(const PointPx& _screenPoint, const PointPx& _anchorPoint, double _angle, double _zx, double _zy, SDL_RendererFlip _flip)
    {
        m_context.set(_screenPoint, _anchorPoint, _angle, _zx, _zy, _flip);
        draw(m_context);
    }

    //------------------------------------------------------------------------------------------

    bool Sprite::animate()
    {
        bool updated = false;
        if (!m_animation.m_run) return updated;

        double delay = m_animation.m_currentFrame > m_animation.m_delays.size() - 1 ? m_animation.m_delays.back() : m_animation.m_delays[m_animation.m_currentFrame];
        if (SDL_GetTicks() - m_animation.m_timer >= delay)
        {
            if (m_animation.m_runningMode == RunningMode::ToRight)
            {
                // new frame
                m_animation.m_currentFrame++;
                updated = true;

                if (m_animation.m_currentFrame >= m_animation.m_frameCount)
                {
                    // end of animation
                    if (m_animation.m_looping)
                    {
                        // loop
                        m_animation.m_currentFrame = 0;
                    }
                    else
                    {
                        // no loop
                        m_animation.m_currentFrame--;
                        m_animation.m_run = false;
                    }
                }
            }
            else
            {
                if (m_animation.m_currentFrame == 0)
                {
                    // end of animation
                    if (m_animation.m_looping)
                    {
                        // loop
                        m_animation.m_currentFrame = m_animation.m_frameCount - 1;
                        updated = true;
                    }
                    else
                    {
                        // no loop
                        m_animation.m_run = false;
                    }
                }
                else
                {
                    m_animation.m_currentFrame--;
                    updated = true;
                }
            }
            m_animation.m_timer = SDL_GetTicks();
        }

        return updated;
    }
}