#ifndef HJARTA_TilesetBuilder_h
#define HJARTA_TilesetBuilder_h

#include "hjarta/serialization/Serializer.h"
#include "hjarta/gfx/Sprite.h"

#include <string>

namespace hjarta
{
    // ## TilesetBuilder
    // Factory to generate an array of pair key - rect
    // Tiles generated could then used to build a set of tiles
    class TilesetBuilder : public Serializable
    {
    public:
        /// ##### Serialization
        void                        serialize(Serializer& _serializer) override;

    public:
        /// ##### Custom types
        struct Tile
        {
            Tile(const std::string& _key, const SDL_Rect& _rect) : key(_key), rect(_rect) {}
            std::string             key;
            SDL_Rect                rect;
        };

    public:
        TilesetBuilder() = default;
        ~TilesetBuilder() = default;

        /// ##### TilesetBuilder interface
        const std::vector<Tile>&    getTiles() const { return m_tiles; }

    private:
        /// ##### Internal methods
        void                        buildTiles();
        std::string                 buildId(int _x, int _y);
        
    private:
        /// ##### Attributes
        std::vector<std::string>    m_colPrefixes;
        std::vector<std::string>    m_rowSuffixes;
        RectPx                      m_init;
        Size2D<int>                 m_size;
        // generated list
        std::vector<Tile>           m_tiles;
    };
}

#endif // HJARTA_TilesetBuilder_h