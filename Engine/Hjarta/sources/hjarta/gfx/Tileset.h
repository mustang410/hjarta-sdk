#ifndef HJARTA_Tileset_h
#define HJARTA_Tileset_h

#include "hjarta/data/Resource.h"
#include "hjarta/data/ResourceProxy.h"
#include "hjarta/gfx/Texture.h"
#include "hjarta/gfx/Sprite.h"
#include "hjarta/gfx/TilesetBuilder.h"

namespace hjarta
{
    // ## Tileset
    // A smart texture binds a texture to a list of sprites
    // Each sprite could be retrieved by its identifier
    class Tileset : public Resource
    {
        HJARTA_DECL_RESOURCE_NON_CLONABLE(hjarta::Tileset, hjarta::Resource)

    public:
        /// ##### Serialization
        void                                    serialize(Serializer& _serializer) override;

    public:
        Tileset() = default;
        ~Tileset();

    public:
        /// ##### Interface
        std::vector<std::string>                verify(const std::vector<std::string>& _ids) const;

        /// ##### Getters
        Texture*                                getTexture() { return m_textureProxy.getResource(); }
        bool                                    hasSprite(const std::string& _id) const { return m_sprites.find(_id) != m_sprites.end(); }
        Sprite*                                 getSprite(const std::string& _id);
        const Sprite*                           getSprite(const std::string& _id) const;

    private:
        /// ##### Internal methods
        void                                    bindTexture();

    private:
        /// ##### Attributes
        ResourceProxy<Texture>                  m_textureProxy;
        std::map<std::string, Sprite>           m_sprites;
        std::map<std::string, TilesetBuilder>   m_tilesets;
        // log missing sprites
        mutable std::set<std::string>           m_missings;
    };
}

#endif // HJARTA_Tileset_h