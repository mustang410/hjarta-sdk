#ifndef HJARTA_Resource_h
#define HJARTA_Resource_h

#include "hjarta/data/ManagedResource.h"

namespace hjarta
{
    class ResourceManager;

    // ## Resource
    // Base class for any resource
    class Resource : public ManagedResource
    {
        HJARTA_DECL_RTTI_ABSTRACT(hjarta::Resource, hjarta::ManagedResource)

    protected:
        // Get a specific resource from the ResourceManager
        // Triggers an assert if there is no associated ResourceManager
        template <class T>
        T* getResource(const SerialURL& _resourceURL);
    };
}

#define HJARTA_DECL_RESOURCE_CLONABLE(Type, ParentType)             \
        HJARTA_DECL_RTTI(Type, ParentType)                          \
    public:                                                         \
        static constexpr bool Clonable = true;                      \
        template <class ...Args>                                    \
        static Type* create(Args ..._args) { return new Type(std::forward<Args>(_args)...); } \
        Type* clone() const { return new Type(*this);}              \
        void clone(Type& _resource) { _resource = Type(*this); }          \

#define HJARTA_DECL_RESOURCE_NON_CLONABLE(Type, ParentType)         \
        HJARTA_DECL_RTTI(Type, ParentType)                          \
    public:                                                         \
        static constexpr bool Clonable = false;                     \
        Type(const Type&) = delete;                                 \
        Type(Type&&) = delete;                                      \
        Type& operator=(const Type&) = delete;                      \
        template <class ...Args>                                    \
        static Type* create(Args ..._args) { return new Type(std::forward<Args>(_args)...); } \
        Type* clone() const                                         \
        {                                                           \
            HJARTA_ASSERT(nullptr, "Resource is not clonable");     \
            return nullptr;                                         \
        }                                                           \
        void clone(Type& _type)                                     \
        {                                                           \
            HJARTA_ASSERT(nullptr, "Resource is not clonable");     \
        }                                                           \

#define HJARTA_DECL_RESOURCE_ABSTRACT(Type, ParentType)             \
        HJARTA_DECL_RTTI_ABSTRACT(Type, ParentType)                 \

#include "hjarta/data/Resource.inl"

#endif // HJARTA_Resource_h