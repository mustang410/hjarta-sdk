#ifndef HJARTA_ResourceManager_inl
#define HJARTA_ResourceManager_inl

namespace hjarta
{
    template <class T, class ...Args>
    Resource* ResourcePartition::createResource(const std::string& _id, Args ..._args)
    {
        static_assert(std::is_base_of<Resource, T>::value, "Class has to inherit from Resource");

        if (m_indexedResources.find(_id) != m_indexedResources.end())
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> already exists for ResourceBundle <", getSerialURL().getNormalizedString(), ">");
            return nullptr;
        }

        Resource* resource = new T(std::forward<Args>(_args)...);
        m_orderedResources.emplace_back(_id, resource);
        m_indexedResources[_id] = resource;
        return resource;
    }

    //------------------------------------------------------------------------------------------

    template <class T> 
    void ResourceManager::registerResourceClass()
    {
        static_assert(std::is_base_of<Resource, T>::value, "ResourceClass has to inherit from Resource");
        registerClass<T>();
    }

    //------------------------------------------------------------------------------------------

    template <class T, class ...Args> 
    T* ResourceManager::createResource(const SerialURL& _partitionURL, const std::string& _id, Args ..._args)
    {
        if (!_partitionURL.isFileURL())
        {
            HJARTA_LOG_ERROR("PartitionURL <", _partitionURL.getNormalizedString(), "> is not a ValueURL");
            return nullptr;
        }

        Resource* resource = m_partitions[_partitionURL].createResource(_id, std::forward<Args>(_args)...);
        resource->setResourceManager(this);
        m_resources[_id] = resource;
        return resource;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    T* ResourceManager::getResource(const std::string& _id)
    {
        auto it = m_resources.find(_id);
        if (it == m_resources.end())
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> is not loaded");
            return nullptr;
        }
        
        T* resource = it->second->dynamicCast<T>();
        if (resource == nullptr)
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> is not a <", T::getClassName(), ">");
            return nullptr;
        }

        return resource;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    const T* ResourceManager::getResource(const std::string& _id) const
    {
        auto it = m_resources.find(_id);
        if (it == m_resources.end())
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> is not loaded");
            return nullptr;
        }

        const T* resource = it->second->dynamicCast<T>();
        if (resource == nullptr)
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> is not a <", T::getClassName(), ">");
            return nullptr;
        }

        return resource;
    }

    template <class T> 
    T& ResourceManager::getSafeResource(const std::string& _id, T& _default)
    {
        T* resource = getResource<T>(_id);
        if (resource != nullptr)
        {
            return *resource;
        }

        return _default;
    }

    //------------------------------------------------------------------------------------------

    template <class T> 
    const T& ResourceManager::getSafeResource(const std::string& _id, T& _default) const
    {
        const T* resource = getResource<T>(_id);
        if (resource != nullptr)
        {
            return *resource;
        }
        
        return _default;
    }
}

#endif // HJARTA_ResourceManager_inl