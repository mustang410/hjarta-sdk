#include "hjarta/data/ResourceManager.h"
#include "hjarta/data/ResourceProxy.h"
#include "hjarta/tools/Log.h"

#include <algorithm>

namespace hjarta
{
    void ResourcePartition::NamedResource::serialize(Serializer& _serializer)
    {
        _serializer.serializeObject("id", &m_id, Serializer::Mandatory, getResourceManager());
        _serializer.serializeObject("resource", &m_resource, Serializer::Mandatory, getResourceManager());
    }

    //------------------------------------------------------------------------------------------

    void ResourcePartition::serialize(Serializer& _serializer)
    {
        // forward manager before serialization
        SerialHookAutoRegister<ManagedResource> prehook(&_serializer, ISerialHook::PreProcess, [&](ManagedResource* _resource) 
        { 
            if (_serializer.isUnserializing())
                _resource->setResourceManager(m_manager); 
        });
        // register resource to manager after serialization
        SerialHookAutoRegister<NamedResource> posthook(&_serializer, ISerialHook::PostProcess, [&](NamedResource* _named)
        { 
            if (_serializer.isUnserializing())
                m_manager->addResource(getSerialURL(), _named->m_id, _named->m_resource);
        });
        _serializer.serializeContainer("resources", &m_orderedResources, Serializer::Mandatory, m_manager);

        if (_serializer.isUnserializing())
        {
            for (NamedResource& named : m_orderedResources)
            {
                m_indexedResources[named.m_id] = named.m_resource;
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void ResourcePartition::removeResource(const std::string& _id)
    {
        if (m_indexedResources.find(_id) == m_indexedResources.end())
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> does not exists for ResourcePartition <", getSerialURL().getNormalizedString(), ">");
            return;
        }

        m_indexedResources.erase(_id);
        auto it = std::find_if(m_orderedResources.begin(), m_orderedResources.end(), [&](const NamedResource& _named) { return _named.m_id == _id; });
        if (it != m_orderedResources.end())
        {
            m_orderedResources.erase(it);
        }
    }

    //------------------------------------------------------------------------------------------

    ResourceManager::ResourceManager(const std::string& _id) : RTTIFactory(_id)
    {

    }

    //------------------------------------------------------------------------------------------

    ResourceManager::~ResourceManager()
    {
        unloadAll();
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::loadPartition(const SerialURL& _partitionURL)
    {
        if (HJARTA_VERIFY(_partitionURL.isFileURL(), "Invalid PartitionURL <", _partitionURL.getNormalizedString(), ">: has to be a FileURL"))
        {
            if (m_partitions.find(_partitionURL) == m_partitions.end())
            {
                m_partitions.emplace(std::make_pair(_partitionURL, ResourcePartition(this)));
                if (m_partitions[_partitionURL].loadFromFileURL(_partitionURL))
                {
                    HJARTA_LOG_INFO("ResourcePartition <", _partitionURL.getNormalizedString(), "> loaded for ResourceManager <", getResourceManagerId(), ">");
                }
                else
                {
                    HJARTA_LOG_ERROR("ResourcePartition <", _partitionURL.getNormalizedString(), "> cannot be loaded for ResourceManager <", getResourceManagerId(), ">");
                }
            }
            else
            {
                HJARTA_LOG_ERROR("ResourcePartition <", _partitionURL.getNormalizedString(), "> already loaded");
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::unloadPartition(const SerialURL& _partitionURL)
    {
        if (HJARTA_VERIFY(_partitionURL.isFileURL(), "Invalid PartitionURL <", _partitionURL.getNormalizedString(), ">: has to be a FileURL"))
        {
            auto it = m_partitions.find(_partitionURL);
            if (it != m_partitions.end())
            {
                for (auto& it : m_partitions[_partitionURL].getIndexedResources())
                {
                    m_resources.erase(it.first);
                }
                m_partitions.erase(it);
            }
            else
            {
                HJARTA_LOG_ERROR("ResourcePartition <", _partitionURL.getNormalizedString(), "> is not loaded");
            }
        }
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::unloadAll()
    {
        for (auto& it : m_resources)
        {
            delete it.second;
        }
        m_resources.clear();
    }

    //------------------------------------------------------------------------------------------

    bool ResourceManager::savePartition(const SerialURL& _partitionURL, const SerialURL& _destURL)
    {
        auto it = m_partitions.find(_partitionURL);
        if (it != m_partitions.end())
        {
            if (it->second.saveToFileURL(_destURL))
            {
                HJARTA_LOG_INFO("ResourcePartition <", _partitionURL.getNormalizedString(), "> saved for ResourceManager <", getResourceManagerId(), ">");
                return true;
            }
        }
        else
        {
            HJARTA_LOG_ERROR("ResourcePartition <", _partitionURL.getNormalizedString(), "> is not loaded");
        }

        return false;
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::saveAll()
    {
        for (auto& it : m_partitions)
        {
            it.second.saveToFileURL(it.first);
        }
        HJARTA_LOG_INFO("All ResourcePartition saved for ResourceManager <", getResourceManagerId(), ">");
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::removeResource(const SerialURL& _partitionURL, const std::string& _id)
    {
        if (!_partitionURL.isFileURL())
        {
            HJARTA_LOG_ERROR("PartitionURL <", _partitionURL.getNormalizedString(), "> is not a FileURL");
            return;
        }

        if (m_partitions.find(_partitionURL) == m_partitions.end())
        {
            HJARTA_LOG_ERROR("PartitionURL <", _partitionURL.getNormalizedString(), "> does not exists for ResourceManager <", getResourceManagerId(), ">");
            return;
        }

        m_partitions[_partitionURL].removeResource(_id);
    }

    //------------------------------------------------------------------------------------------

    void ResourceManager::addResource(const SerialURL& _partitionURL, const std::string& _id, Resource* _resource)
    {
        if (_resource == nullptr)
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> cannot be loaded for ResourcePartition <", _partitionURL.getNormalizedString(), ">");
            return;
        }

        if (m_resources.find(_id) == m_resources.end())
        {
            m_resources[_id] = _resource;
        }
        else
        {
            HJARTA_LOG_ERROR("ResourceId <", _id, "> already loaded for ResourcePartition <", _partitionURL.getNormalizedString(), ">");
        }
    }
}