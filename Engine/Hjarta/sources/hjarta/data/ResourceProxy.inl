#ifndef HJARTA_ResourceProxy_inl
#define HJARTA_ResourceProxy_inl

#include "hjarta/data/ResourceManager.h"

HJARTA_DECL_ENUM_HELPER(hjarta::ResourceProxyType, {
    { hjarta::ResourceProxyType::Empty, "empty" },
    { hjarta::ResourceProxyType::Link, "link" },
    { hjarta::ResourceProxyType::Clone, "clone" },
    { hjarta::ResourceProxyType::Raw, "raw" }
});

namespace hjarta
{
    template <class T>
    void ResourceProxy<T>::serialize(Serializer& _serializer)
    {
        if (_serializer.isUnserializing())
        {
            m_type = ResourceProxyType::Link; // default value
        }
        _serializer.serializeEnum("type", &m_type, Serializer::Optional);
        switch (m_type)
        {
        case ResourceProxyType::Link:
        case ResourceProxyType::Clone:
            _serializer.serializeObject("resource_id", &m_resourceId);
            break;

        case ResourceProxyType::Raw:
            m_resource = T::create();
            _serializer.serializeObject("resource", m_resource);
            break;
        }

        if (_serializer.isUnserializing())
        {
            HJARTA_ASSERT(m_type != ResourceProxyType::Shared, "Cannot unserialized a Shared resource");
            if (m_type == ResourceProxyType::Link || m_type == ResourceProxyType::Clone)
            {
            
                if (HJARTA_VERIFY(hasResourceManager(), "ResourceProxy has to be managed to clone from ResourceId <", m_resourceId, ">"))
                {
                    if (T* resource = getResourceManager()->getResource<T>(m_resourceId))
                    {
                        if (m_type == ResourceProxyType::Clone)
                        {
                            m_resource = resource->clone();
                        }
                    }
                    else
                    {
                        HJARTA_LOG_ERROR("ResourceId <", m_resourceId, "> could not be loaded from <", getResourceManager()->getResourceManagerId(), ">");
                    }
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    ResourceProxy<T>::ResourceProxy(const ResourceProxy& _proxy) :
        ManagedResource(_proxy)
    {
        m_type = _proxy.m_type;
        switch (m_type)
        {
        case ResourceProxyType::Link:
            m_resourceId = _proxy.m_resourceId;
            break;

        case ResourceProxyType::Shared:
            m_resource = const_cast<ResourceProxy&>(_proxy).getResource();
            break;

        case ResourceProxyType::Clone:
            m_resourceId = _proxy.m_resourceId;
        case ResourceProxyType::Raw:
            if (const T* resource = _proxy.getResource())
            {
                m_resource = resource->clone();
            }
            break;
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    ResourceProxy<T>::~ResourceProxy()
    {
        clearResource();
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    T* ResourceProxy<T>::getResource()
    {
        switch (m_type)
        {
        case ResourceProxyType::Link:
            return getResourceManager()->getResource<T>(m_resourceId);
        case ResourceProxyType::Shared:
        case ResourceProxyType::Clone:
        case ResourceProxyType::Raw:
            return m_resource;
        }

        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    const T* ResourceProxy<T>::getResource() const
    {
        switch (m_type)
        {
        case ResourceProxyType::Link:
            if (HJARTA_VERIFY(hasResourceManager(), "ResourceProxy has to be managed to clone from ResourceId <", m_resourceId, ">"))
                return getResourceManager()->getResource<T>(m_resourceId);
        case ResourceProxyType::Shared:
        case ResourceProxyType::Clone:
        case ResourceProxyType::Raw:
            return m_resource;
        }

        return nullptr;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    T& ResourceProxy<T>::getSafeResource(const std::string& _errorMessage, T& _default)
    {
        if (T* resource = getResource())
        {
            return *resource;
        }

        if (!_errorMessage.empty())
            HJARTA_LOG_ERROR(_errorMessage);

        return _default;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    const T& ResourceProxy<T>::getSafeResource(const std::string& _errorMessage, T& _default) const
    {
        if (const T* resource = getResource())
        {
            return *resource;
        }

        if (!_errorMessage.empty())
            HJARTA_LOG_ERROR(_errorMessage);

        return _default;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void ResourceProxy<T>::clearResource()
    {
        if (m_type != ResourceProxyType::Empty)
        {
            m_resourceId = std::string{};
            if (m_resource != nullptr && m_type != ResourceProxyType::Shared)
            {
                delete m_resource;
            }
            m_resource = nullptr;
            m_type = ResourceProxyType::Empty;
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void ResourceProxy<T>::linkResource(const std::string& _resourceId)
    {
        clearResource();

        if (!hasResourceManager())
        {
            HJARTA_ASSERT(nullptr, "Cannot share ResourceId <", _resourceId, "> for unmanaged ResourceProxy");
            return;
        }

        T* resource = getResourceManager()->getResource<T>(_resourceId);
        if (resource == nullptr)
        {
            HJARTA_LOG_ERROR("Cannot find ResourceId <", _resourceId, "> from ResourceManager <", m_manager->getResourceManagerId(), ">");
            return;
        }

        m_resourceId = _resourceId;
        m_type = ResourceProxyType::Link;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void ResourceProxy<T>::shareResource(T* _resource)
    {
        clearResource();

        m_resource = _resource;
        m_type = ResourceProxyType::Shared;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void ResourceProxy<T>::cloneResource(const std::string& _resourceId)
    {
        clearResource();

        if (hasResourceManager())
        {
            HJARTA_ASSERT(nullptr, "Cannot clone ResourceId <", _resourceId, "> for unmanaged ResourceProxy");
            return;
        }

        T* resource = m_manager->getResource<T>(_resourceId);
        if (!T::Clonable)
        {
            HJARTA_ASSERT(nullptr, "Attempting to clone non clonable ResourceId <", _resourceId, "> from ResourceManager <", m_manager->getResourceManagerId(), ">");
            return nullptr;
        }

        m_resourceId = _resourceId;
        m_resource = resource->clone();
        m_type = ResourceProxyType::Clone;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void ResourceProxy<T>::cloneResource(const T& _resource)
    {
        clearResource();

        m_resource = _resource.clone();
        m_type = ResourceProxyType::Raw;
    }



    //------------------------------------------------------------------------------------------

    template <class T>
    template <class ...Args>
    void ResourceProxy<T>::createResource(Args ..._args)
    {
        clearResource();

        m_resource = T::create(std::forward<Args>(_args)...);
        m_type = ResourceProxyType::Raw;
    }
}

#endif // HJARTA_ResourceProxy_inl