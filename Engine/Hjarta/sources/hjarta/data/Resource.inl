#ifndef HJARTA_Resource_inl
#define HJARTA_Resource_inl

namespace hjarta
{
    template <class T>
    T* Resource::getResource(const SerialURL& _resourceURL)
    {
        if (hasResourceManager())
        {
            HJARTA_LOG_ERROR("There is no associated ResourceManager to resource <", _resourceURL.getNormalizedString(), ">");
            return nullptr;
        }

        return getResourceManager->getResource<T>(_resourceURL);;
    }
}

#endif // HJARTA_Resource_inl