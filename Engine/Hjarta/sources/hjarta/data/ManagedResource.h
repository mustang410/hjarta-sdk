#ifndef HJARTA_ManagedResource_h
#define HJARTA_ManagedResource_h

#include "hjarta/serialization/Serializer.h"

namespace hjarta
{
    class ResourceManager;
    // ## ManagedResource
    // Define ResourceManager link logic
    class ManagedResource : public Serializable
    {
        HJARTA_DECL_RTTI_ABSTRACT(hjarta::ManagedResource, hjarta::Serializable);

    public:
        ManagedResource(ResourceManager* _manager = nullptr) : m_manager(_manager) {}

    public:
        /// ##### ResourceBase interface
        void                    setResourceManager(ResourceManager* _manager) { m_manager = _manager; }
        bool                    hasResourceManager() const { return m_manager != nullptr; }
        ResourceManager*        getResourceManager() { return m_manager; }
        const ResourceManager*  getResourceManager() const { return m_manager; }

    private:
        /// ##### Attributes
        ResourceManager*        m_manager = nullptr;
    };
}

#endif // HJARTA_ManagedResource_h