#ifndef HJARTA_ResourceManager_h
#define HJARTA_ResourceManager_h

#include "hjarta/data/Resource.h"

#include <vector>
#include <map>
#include <set>

#define HJARTA_RESOURCE_MANAGER() hjarta::InstanceProvider<hjarta::ResourceManager>::get()

namespace hjarta
{
    class ResourceManager;

    // ## ResourcePartition
    // Regroup a bunch of resources
    class ResourcePartition : public Serializable
    {
        HJARTA_DECL_RTTI(hjarta::ResourcePartition, hjarta::Serializable)

    public:
        /// ##### Serialization
        void                                        serialize(Serializer& _serializer);

    public:
        ResourcePartition() = default;
        ResourcePartition(ResourceManager* _manager) : m_manager(_manager) {}

    public:
        /// ##### ResourceBundle interface
        template <class T, class ...Args>
        Resource*                                   createResource(const std::string& _id, Args ..._args);
        std::map<std::string, Resource*>&           getIndexedResources() { return m_indexedResources; }
        void                                        removeResource(const std::string& _id);

    private:
        // ## NamedResource
        // Associate an id to a resource
        // Used for ordered serialization
        struct NamedResource : public ManagedResource
        {
            HJARTA_DECL_RTTI_ABSTRACT(hjarta::ResourcePartition::NamedResource, hjarta::ManagedResource);

            /// ##### Serialization
            void                                    serialize(Serializer& _serializer) override;
            NamedResource() = default;
            NamedResource(const std::string& _id, Resource* _resource) : m_id(_id), m_resource(_resource) {}
            ~NamedResource() { if (m_resource != nullptr) delete m_resource; }
            std::string                             m_id;
            Resource*                               m_resource = nullptr;
        };

    private:
        /// ##### Attributes
        ResourceManager*                            m_manager;
        std::vector<NamedResource>                  m_orderedResources;
        std::map<std::string, Resource*>            m_indexedResources;
    };

    // ## ResourceManager
    // Central point for any resource management
    // Resources are loaded / unloaded by bundle
    // Once a resource is loaded, it can ba accessed through its ResourceId
    class ResourceManager : public RTTIFactory
    {
    public:
        ResourceManager(const std::string& _id);
        ~ResourceManager();

    public:
        /// ##### ResourceManager interface
        const std::string&                          getResourceManagerId() const { return getFactoryId(); }
        template <class T> void                     registerResourceClass();
        void                                        loadPartition(const SerialURL& _partitionURL);
        void                                        unloadPartition(const SerialURL& _partitionURL);
        void                                        unloadAll();
        bool                                        savePartition(const SerialURL& _partitionURL) { return savePartition(_partitionURL, _partitionURL); }
        bool                                        savePartition(const SerialURL& _partitionURL, const SerialURL& _destURL);
        void                                        saveAll();
        template <class T, class ...Args> T*        createResource(const SerialURL& _partitionURL, const std::string& _id, Args ..._args);
        template <class T> T*                       getResource(const std::string& _id);
        template <class T> const T*                 getResource(const std::string& _id) const;
        template <class T> T&                       getSafeResource(const std::string& _id, T& _default = T{});
        template <class T> const T&                 getSafeResource(const std::string& _id, T& _default = T{}) const;
        void                                        removeResource(const SerialURL& _partitionURL, const std::string& _id);

    private:
        friend class ResourcePartition;
        void                                        addResource(const SerialURL& _partitionURL, const std::string& _id, Resource* _resource);


    private:
        /// ##### Attributes
        std::map<SerialURL, ResourcePartition>      m_partitions;
        std::map<std::string, Resource*>            m_resources;
    };
}

#include "hjarta/data/ResourceManager.inl"

#endif // HJARTA_ResourceManager_h