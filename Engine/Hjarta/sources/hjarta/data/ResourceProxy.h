#ifndef HJARTA_ResourceProxy_h
#define HJARTA_ResourceProxy_h

#include "hjarta/serialization/Serializer.h"
#include "hjarta/data/Resource.h"

namespace hjarta
{
    class ResourceManager;

    enum class ResourceProxyType {
        Empty,
        Link,
        Shared,
        Clone,
        Raw,
        Count
    };

    // ## ResourceProxy
    // Handle ether a ResourceId or a Resource
    template <class T>
    class ResourceProxy : public ManagedResource
    {
        HJARTA_DECL_RTTI(hjarta::ResourceProxy<T>, hjarta::ManagedResource)

    public:
        /// ##### Serialization
        void                            serialize(Serializer& _serializer) override;

    public:
        static_assert(std::is_base_of<Resource, T>::value, "Class has to inherit from Resource");
        ResourceProxy() = default;
        ResourceProxy(const ResourceProxy& _proxy);
        ~ResourceProxy();

    public:
        /// ##### Operators
        operator bool() const { return hasResource(); }
        T* operator->() { return getResource(); }
        const T* operator->() const { return getResource(); }

    public:
        /// ##### ResourceProxy interface
        // Resource
        ResourceProxyType               getProxyType() const { return m_type; }
        bool                            hasResource() const { return getResource() != nullptr; }
        T*                              getResource();
        const T*                        getResource() const;
        T&                              getSafeResource(const std::string& _errorMessage = std::string{}, T& _default = T{});
        const T&                        getSafeResource(const std::string& _errorMessage = std::string{}, T& _default = T{}) const;
        void                            clearResource();
        // Set
        void                            linkResource(const std::string& _resourceId);
        void                            shareResource(T* _resource);
        void                            cloneResource(const std::string& _resourceId);
        void                            cloneResource(const T& _resource);
        template <class ...Args> void   createResource(Args ..._args);

    private:
        /// ##### Attributes
        ResourceProxyType               m_type = ResourceProxyType::Empty;
        std::string                     m_resourceId;
        T*                              m_resource = nullptr;
    };
}

#include "hjarta/data/ResourceProxy.inl"

#endif // HJARTA_ResourceProxy_h