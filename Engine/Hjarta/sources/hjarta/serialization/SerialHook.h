#ifndef HJARTA_SerialHook_h
#define HJARTA_SerialHook_h

#include "hjarta/core/RTTI.h"

#include <set>
#include <functional>

namespace hjarta
{
    // ## ISerialHook
    // Base class for serialization hooks
    // A serialization hook is applicable to a RTTI class
    class ISerialHook
    {
    public:
        /// ##### Custom types
        enum Type { PreProcess, PostProcess };
        template <class T> using Callback = std::function<void(T*)>;

    public:
        ISerialHook() = default;
        ISerialHook(Type _type) : m_type(_type) {}

    public:
        /// ##### IHook interface
        virtual void                    process(RTTI* _object) = 0;
        virtual RTTI::TypeId            getClassId() const = 0;
        Type                            getType() const { return m_type; }

    private:
        /// ##### Attributes
        Type                            m_type;
    };

    // ## SerialHook
    // Hook for a specific class
    template <class T> 
    class SerialHook : public ISerialHook
    {
    public:
        SerialHook() = default;
        SerialHook(Type _type, const Callback<T>& _callback) : ISerialHook(_type), m_callback(_callback) {}

    public:
        /// ##### Hook interface
        void                            process(RTTI* _rtti) override;
        RTTI::TypeId                    getClassId() const override { return T::getClassId(); }

    private:
        /// ##### Attributes
        Callback<T>                     m_callback = nullptr;
    };

    // ## SerialHooker
    // Base class for serialization hooks handling
    class SerialHooker
    {
    public:
        SerialHooker() = default;
        ~SerialHooker();

    public:
        /// ##### SerialHooker interface
        template <class T> ISerialHook* addHook(ISerialHook::Type _type, const ISerialHook::Callback<T>& _callback);
        void                            removeHook(ISerialHook* _hook);
        template <class T> void         checkHooks(ISerialHook::Type _type, T* _object);
    private:
        /// ##### Attributes
        std::set<ISerialHook*>          m_hooks;
    };

    // ## SerialHookAutoRegister
    // Automatically create a hook and remove it at destruction
    template <class T>
    class SerialHookAutoRegister
    {
    public:
        SerialHookAutoRegister(SerialHooker* _hooker, ISerialHook::Type _type, const ISerialHook::Callback<T>& _callback);
        ~SerialHookAutoRegister();

    private:
        SerialHooker*                   m_hooker = nullptr;
        ISerialHook*                    m_hook = nullptr;
    };
}

#include "hjarta/serialization/SerialHook.inl"

#endif // HJARTA_SerialHook_h