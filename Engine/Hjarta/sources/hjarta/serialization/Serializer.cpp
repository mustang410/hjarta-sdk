#include "hjarta\serialization\Serializer.h"

#include "hjarta/tools/Log.h"

namespace hjarta
{
    bool Serializable::loadFromFileURL(const SerialURL& _url)
    {
        Archive archive(_url);
        if (archive.isLoaded())
        {
            Serializer serializer(archive);
            m_url = _url;
            serialize(serializer);
            return true;
        }

        return false;
    }

    //------------------------------------------------------------------------------------------

    bool Serializable::saveToFileURL(const SerialURL& _url)
    {
        Archive archive;
        Serializer serializer(archive);
        m_url = _url;
        serialize(serializer);
        return archive.save(_url);
    }

    //------------------------------------------------------------------------------------------

    const std::string Serializer::RTTIClassNameKey = "class_name";

    //------------------------------------------------------------------------------------------

    Serializer::Serializer(Archive &_archive) :
        m_root(_archive.getJsonValue())
    {
        if (_archive.isLoaded())
            m_mode = Mode::Unserialize;
        else if (!_archive.isFaulty())
            m_mode = Mode::Serialize;
        else
        {
            HJARTA_LOG_ERROR("Unable to create a serializer from archive <", _archive.getURL().getNormalizedString(), ">");
            m_mode = Mode::Unset;
        }

        m_context.m_currentJson = &m_root;
        m_context.m_currentURL = _archive.getURL();
    }

    //------------------------------------------------------------------------------------------

    bool Serializer::nextObject(const std::string& _key, ParameterType _type)
    {
        if (checkKey(_key, _type))
        {
            m_context.m_currentJson = &(*m_context.m_currentJson)[_key];
            m_context.m_currentURL = m_context.m_currentURL.appendNode(_key);
            return true;
        }

        return false;
    }

    //------------------------------------------------------------------------------------------

    void Serializer::nextContainerObject(size_t _index, Json::Value& _item)
    {
        m_context.m_currentJson = &_item;
        m_context.m_currentURL = m_context.m_currentURL.appendNode(std::to_string(_index));
    }

    //------------------------------------------------------------------------------------------

    bool Serializer::checkKey(const std::string &_key, ParameterType _type)
    {
        if (!isUnserializing())
            return true;;

        if (!m_context.m_currentJson->isObject())
        {
            if (_type == ParameterType::Mandatory)
            {
                HJARTA_LOG_ERROR("Invalid json format! Object at <", getCurrentURL().getNormalizedString(), "> is not an object!");
                HJARTA_LOG_ERROR("Looking for key <", _key, ">");
            }
            
            return false;
        }

        if ((*m_context.m_currentJson)[_key].type() == Json::nullValue)
        {
            if (_type == ParameterType::Mandatory)
            {
                HJARTA_LOG_ERROR("Invalid json format! Missing child for <", getCurrentURL().getNormalizedString(), ">");
                HJARTA_LOG_ERROR("Looking for key <", _key, ">");
            }
            return false;
        }

         return true;
    }
}