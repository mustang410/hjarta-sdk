#ifndef HJARTA_SerialHook_inl
#define HJARTA_SerialHook_inl

namespace hjarta
{
    template <class T>
    void SerialHook<T>::process(RTTI* _rtti)
    {
        if (T* object = _rtti->dynamicCast<T>())
        {
            if (m_callback)
                m_callback(object);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    ISerialHook* SerialHooker::addHook(ISerialHook::Type _type, const ISerialHook::Callback<T>& _callback)
    {
        ISerialHook* hook = new SerialHook<T>(_type, _callback);
        m_hooks.emplace(hook);
        return hook;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void SerialHooker::checkHooks(ISerialHook::Type _type, T* _object)
    {
        if (_object)
        {
            for (ISerialHook* hook : m_hooks)
            {
                if (hook->getType() == _type)
                {
                    if (_object->isClass(hook->getClassId()))
                    {
                        hook->process(_object);
                    }
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    SerialHookAutoRegister<T>::SerialHookAutoRegister(SerialHooker* _hooker, ISerialHook::Type _type, const ISerialHook::Callback<T>& _callback)
    {
        m_hooker = _hooker;
        m_hook = m_hooker->addHook(_type, _callback);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    SerialHookAutoRegister<T>::~SerialHookAutoRegister()
    {
        m_hooker->removeHook(m_hook);
    }
}

#endif // HJARTA_SerialHook_inl