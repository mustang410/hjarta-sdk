#ifndef HJARTA_Serializer_inl
#define HJARTA_Serializer_inl

#include "hjarta/tools/Log.h"
#include "hjarta/core/Enum.h"

namespace hjarta
{
    template <class T>
    void Serializer::serializeSimple(T* _value, RTTIFactory* _factory)
    {
        serializeInternal(_value, _factory);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeObject(const std::string& _key, T* _value, ParameterType _type, RTTIFactory* _factory)
    {
        SerialScopeContext scope(m_context);
        if (nextObject(_key, _type))
        {
            presetCurrentObject<T>();
            serializeInternal(_value, _factory);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeEnum(const std::string& _key, T* _enum, ParameterType _type)
    {
        static_assert(EnumHelper<T>::Implemented, "EnumHelper is not supported for this enum");

        EnumSerializable<T> serializable(_enum);
        serializeObject(_key, &serializable, _type);
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeContainer(const std::string& _key, std::vector<T> *_container, ParameterType _type, RTTIFactory* _factory)
    {
        SerialScopeContext scope(m_context);
        if (nextObject(_key, _type))
        {
            presetCurrentContainer<T>(*_container);
            size_t index = 0;
            Json::Value& items = getCurrentJson();
            for (Json::Value& item : items)
            {
                SerialScopeContext scope(m_context);
                nextContainerObject(index, item);
                serializeInternal(&(*_container)[index], _factory);
                ++index;
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class Key, class T>
    void Serializer::serializeMap(const std::string& _key, std::map<Key, T> *_map, ParameterType _type, RTTIFactory* _factory)
    {
        SerialScopeContext scope(m_context);
        if (nextObject(_key, _type))
        {
            std::vector<Key> keys = computeMapKeys(*_map);
            for (Key& key : keys)
            {
                SerialScopeContext subscope(m_context);
                if (nextObject(std::to_string(key), Optional))
                {
                    serializeInternal(&(*_map)[key], _factory);
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeInternal(T* _object, RTTIFactory* _factory)
    {
        static_assert(std::is_base_of<ISerializable, T>::value, "Class has to inherit from Serializable");

        if (isUnserializing())
        {
            if (_object != nullptr)
            {
                _object->setSerialURL(getCurrentURL());
            }
        }

        if (_object != nullptr)
        {
            checkHooks(ISerialHook::PreProcess, _object);
            _object->serialize(*this);
            checkHooks(ISerialHook::PostProcess, _object);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeInternal(T** _object, RTTIFactory* _factory)
    {
        static_assert(std::is_base_of<ISerializable, T>::value, "Class has to inherit from Serializable");

        if (isUnserializing())
        {
            if (*_object == nullptr && _factory != nullptr)
            {
                *_object = buildClass<T>(getCurrentJson(), _factory);
            }

            if (*_object != nullptr)
            {
                (*_object)->setSerialURL(getCurrentURL());
            }
            else
            {
                HJARTA_LOG_ERROR("Could not build SerialURL <", getCurrentURL().getNormalizedString(), "> from RTTI factory");
            }
        }
        else if (isSerializing())
        {
            if (*_object != nullptr)
            {
                std::string className((*_object)->getClassInstanceName());
                serializeObject(RTTIClassNameKey, &className);
            }
        }

        if (*_object != nullptr)
        {
            checkHooks(ISerialHook::PreProcess, *_object);
            (*_object)->serialize(*this);
            checkHooks(ISerialHook::PostProcess, *_object);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    void Serializer::serializeInternalGeneric(T* _value)
    {
        if (isSerializing())
        {
            getCurrentJson() = (Json::Value)(*_value);
        }
        else if (isUnserializing())
        {
            if (!m_context.m_currentJson->isNull())
            {
                if (isGenericType<T>(getCurrentJson()))
                    *_value = convert<T>(getCurrentJson());
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T> 
    void Serializer::presetCurrentObject()
    {
        if (isSerializing() && !isGeneric<T>())
        {
            *m_context.m_currentJson = Json::Value(Json::objectValue);
        }
    }

    //------------------------------------------------------------------------------------------

    template <class T> 
    void Serializer::presetCurrentContainer(std::vector<T>& _container)
    {
        if (isSerializing())
        {
            getCurrentJson() = Json::Value(Json::arrayValue);
            for (unsigned i = getCurrentJson().size(); i < _container.size(); i++)
            {
                getCurrentJson().append(Json::Value());
            }
        }
        else if (isUnserializing())
        {
            for (size_t i = _container.size(); i < getCurrentJson().size(); i++)
            {
                _container.emplace_back();
            }
        }
    }

    //------------------------------------------------------------------------------------------

    template <class Key, class T> 
    std::vector<Key> Serializer::computeMapKeys(std::map<Key, T>& _map)
    {
        std::vector<Key> keys;
        if (isUnserializing())
        {
            for (const std::string& key : getCurrentJson().getMemberNames())
                keys.emplace_back(key);
        }
        else if (isSerializing())
        {
            for (auto &it : _map)
                keys.emplace_back(it.first);
        }
        return keys;
    }

    //------------------------------------------------------------------------------------------

    template <class T>
    T* Serializer::buildClass(Json::Value& _value, RTTIFactory* _factory)
    {
        if (_value.isObject())
        {
            if (getCurrentJsonChild(RTTIClassNameKey).isString())
            {
                const char* className = getCurrentJsonChild(RTTIClassNameKey).asCString();
                RTTI* rtti = _factory->buildClass(className);
                if (rtti)
                {
                    return rtti->dynamicCast<T>();
                }
            }
        }

        return nullptr;
    }
}

#endif // HJARTA_Serializer_inl