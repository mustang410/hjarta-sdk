#include "hjarta\serialization\Archive.h"

#include "hjarta/tools/Log.h"

#include <fstream>

namespace hjarta
{
    Archive::Archive(const SerialURL& _url)
    {
        load(_url);
    }

    //------------------------------------------------------------------------------------------
    
    Archive::Archive(const Archive& _copy) :
        m_status(_copy.m_status),
        m_url(_copy.m_url),
        m_value(_copy.m_value)
    {

    }

    //------------------------------------------------------------------------------------------

    bool Archive::load(const SerialURL& _url)
    {
        if (!_url.isFileURL())
        {
            HJARTA_LOG_ERROR("URL <", _url.getNormalizedString(), "> is not a FileURL");
            return false;
        }
        m_url = _url;

        // loading file
        std::ifstream file(m_url.getFileName());
        if (!file.is_open())
        {
            HJARTA_LOG_ERROR("Unable to open archive <", m_url.getNormalizedString(), "> ");

            m_status = Status::Faulty;
            return false;
        }

        // parsing file
        Json::Reader reader;
        if (!reader.parse(file, m_value, false))
        {
            // unable to parse file
            HJARTA_LOG_ERROR("Unable to load archive <", m_url.getNormalizedString(), "> ");
            HJARTA_LOG_ERROR("Errors: ");
            HJARTA_LOG_ERROR(reader.getFormattedErrorMessages());

            m_status = Status::Faulty;
            return false;
        }

        HJARTA_LOG_INFO("Archive <", m_url.getNormalizedString(), "> loaded");
        m_status = Status::Loaded;
        return true;
    }

    //------------------------------------------------------------------------------------------

    bool Archive::save(const SerialURL& _url)
    {
        if (!_url.isFileURL())
        {
            HJARTA_LOG_ERROR("URL <", _url.getNormalizedString(), "> is not a FileURL");
            return false;
        }

        m_url = _url;

        // create file content
        Json::StyledWriter writer;
        std::string content = writer.write(m_value);

        // loading file
        std::ofstream file(m_url.getFileName());

        if (!file.is_open())
        {
            // unable to open file
            HJARTA_LOG_ERROR("Unable to save archive <", m_url.getNormalizedString(), "> ");
            m_status = Status::Faulty;
            return false;
        }

        // save content
        file << content;
        file.close();

        HJARTA_LOG_INFO("Archive <", m_url.getNormalizedString(), "> saved");
        m_status = Status::Saved;
        return true;
    }
}