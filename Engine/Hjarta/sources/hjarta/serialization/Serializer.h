#ifndef HJARTA_Serializer_h
#define HJARTA_Serializer_h

#include "hjarta/serialization/jsoncpp/json.h"
#include "hjarta/serialization/Archive.h"
#include "hjarta/serialization/SerialURL.h"
#include "hjarta/serialization/ISerializable.h"
#include "hjarta/serialization/SerialContext.h"
#include "hjarta/serialization/SerialHook.h"
#include "hjarta/core/StringId.h"
#include "hjarta/core/RTTI.h"
#include "hjarta/core/RTTIFactory.h"

#include "SDL.h"
#include <map>

namespace hjarta
{
    // ### Serializable
    // Interface to be implemented by any serializable object
    // Any inherited class should implement this method
    // The serializer provides a set of public method to serialize class members:
    // For generics, serializables,, containers, etc.
    class Serializer;
    class Serializable : public RTTI, public ISerializable
    {
        HJARTA_DECL_RTTI_ABSTRACT(hjarta::Serializable, hjarta::RTTI)

    public:
        /// ##### Serialization
        // To implement
        virtual void                serialize(Serializer& _serializer) = 0;
        // From file
        bool                        loadFromFileURL(const SerialURL& _url);
        bool                        saveToFileURL(const SerialURL& _url);

    public:
        /// ##### Serializable interface
        const SerialURL&            getSerialURL() const { return m_url; }

    private:
        // only Serializer is able to set a Serializable URL
        friend class Serializer;
        void                        setSerialURL(const SerialURL& _url) override { m_url = _url; }

    private:
        /// ##### Attributes
        SerialURL                   m_url;
    };

    // ## Serializer
    // Serialization tool to serialize classes into json archives
    class Serializer : public SerialHooker
    {
    public:
        /// ##### Custom types
        enum class Mode { Unset, Serialize, Unserialize };
        enum ParameterType { Mandatory, Optional };

    public:
        // A serializer has to be linked to an archive
        // The archive must be alive during the serializer ttl
        Serializer(Archive& _archive);

        /// ##### Status
        bool                        isSerializing() const { return m_mode == Mode::Serialize; }
        bool                        isUnserializing() const { return m_mode == Mode::Unserialize; }

    public:
        /// ##### Serialization methods
        // Set _warn to false do disable log error if value is missing
        template <class T> void     serializeSimple(T* _value, RTTIFactory* _factory = nullptr);
        template <class T> void     serializeObject(const std::string& _key, T* _value, ParameterType _type = Mandatory, RTTIFactory* _factory = nullptr);
        template <class T> void     serializeEnum(const std::string& _key, T* _enum, ParameterType _type = Mandatory);
        template <class T> void     serializeContainer(const std::string& _key, std::vector<T>* _container, ParameterType _type = Mandatory, RTTIFactory* _factory = nullptr);
        template <class Key, class T> void serializeMap(const std::string& _key, std::map<Key, T>* _map, ParameterType _type = Mandatory, RTTIFactory* _factory = nullptr);

    private:
        /// ##### Base method
        // Serialize / unserialize from the current json value
        template <class T> void     serializeInternal(T* _value, RTTIFactory* _factory);
        template <class T> void     serializeInternal(T** _value, RTTIFactory* _factory);
        // for generic types
        template <class T> void     serializeInternalGeneric(T* _value);
        template <> void            serializeInternal(bool* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <> void            serializeInternal(unsigned* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <> void            serializeInternal(Uint8* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <> void            serializeInternal(int* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <> void            serializeInternal(std::string* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <> void            serializeInternal(float* _value, RTTIFactory*) { serializeInternalGeneric(_value); }
        template <>  void           serializeInternal(double* _value, RTTIFactory*) { serializeInternalGeneric(_value); }

    private:
        /// ##### Initializations
        template <class T> void     presetCurrentObject();
        template <class T> void     presetCurrentContainer(std::vector<T>& _container);
        template <class Key, class T> std::vector<Key> computeMapKeys(std::map<Key, T>& _map);
        static const std::string    RTTIClassNameKey;
        template <class T> T*       buildClass(Json::Value& _value, RTTIFactory* _factory);

    private:
        /// ##### Check if a type is generic or not
        template <class T> bool     isGeneric() const { return false; }
        template <> bool            isGeneric<bool>() const { return true; }
        template <> bool            isGeneric<unsigned>() const { return true; }
        template <> bool            isGeneric<Uint8>() const { return true; }
        template <> bool            isGeneric<int>() const { return true; }
        template <> bool            isGeneric<std::string>() const { return true; }
        template <> bool            isGeneric<float>() const { return true; }
        template <> bool            isGeneric<double>() const { return true; }

        /// ##### Json helpers
        template <class T> T        convert(Json::Value& _value) const { return T{}; }
        template <> bool            convert(Json::Value& _value) const { return _value.asBool(); }
        template <> Uint8           convert(Json::Value& _value) const { return _value.asUInt(); }
        template <> unsigned        convert(Json::Value& _value) const { return _value.asUInt(); }
        template <> int             convert(Json::Value& _value) const { return _value.asInt(); }
        template <> double          convert(Json::Value& _value) const { return _value.asDouble(); }
        template <> float           convert(Json::Value& _value) const { return _value.asFloat(); }
        template <> std::string     convert(Json::Value& _value) const { return _value.asString(); }

        template <class T> bool     isGenericType(Json::Value& _value) const { return false; }
        template <> bool            isGenericType<bool>(Json::Value& _value) const { return _value.isBool() || _value.isInt() || _value.isUInt(); }
        template <> bool            isGenericType<unsigned>(Json::Value& _value) const { return _value.isUInt() || _value.isInt(); }
        template <> bool            isGenericType<Uint8>(Json::Value& _value) const { return _value.isUInt() || _value.isInt(); }
        template <> bool            isGenericType<int>(Json::Value& _value) const { return _value.isInt() || _value.isUInt(); }
        template <> bool            isGenericType<double>(Json::Value& _value) const { return _value.isDouble() || _value.isInt() || _value.isUInt(); }
        template <> bool            isGenericType<float>(Json::Value& _value) const { return _value.isDouble(); }
        template <> bool            isGenericType<std::string>(Json::Value& _value) const { return _value.isString(); }

    private:
        /// ##### Helpers
        Json::Value&                getCurrentJson() { return *m_context.m_currentJson; }
        Json::Value&                getCurrentJsonChild(const std::string& _key) { return (*m_context.m_currentJson)[_key]; }
        SerialURL&                  getCurrentURL() { return m_context.m_currentURL; }
        bool                        nextObject(const std::string& _key, ParameterType _type);
        void                        nextContainerObject(size_t _index, Json::Value& _item);
        bool                        checkKey(const std::string& _key, ParameterType _type);

    private:
        /// ##### Attributes
        Mode                        m_mode = Mode::Unset;
        Json::Value&                m_root;
        SerialContext               m_context;
    };
}

#include "hjarta\serialization\Serializer.inl"

#endif // HJARTA_Serializer_h