#ifndef HJARTA_SerialURL_h
#define HJARTA_SerialURL_h

#include "hjarta/serialization/ISerializable.h"
#include "hjarta/core/RTTI.h"

#include <vector>

namespace hjarta
{
    // ## SerialURL
    // URL to a resource
    // Can be either a file URL or a value URL
    // Format examples:
    // - for a file: dir/subdir/file_name.ext
    // - for a value: dir/file_name.ext:dict/object
    class SerialURL : public RTTI, public ISerializable
    {
        HJARTA_DECL_RTTI(hjarta::SerialURL, hjarta::RTTI);

    public:
        /// ##### Serialization
        void                        serialize(Serializer& _serializer) override;

    public:
        /// ##### Custom types
        static const char           NodeSeparator;
        static const char           FileDelimiter;

    public:
        SerialURL() = default;
        SerialURL(const char* _serialURL) : SerialURL(std::string(_serialURL)) {}
        SerialURL(const std::string& _url);

    public:
        /// ##### SerialURL interface
        void                        set(const std::string& _url);
        void                        clear();
        SerialURL                   appendNode(const std::string& _node) const;
        SerialURL                   appendRelativeURL(const SerialURL& _relativeURL) const;
        bool                        isFileURL() const { return !isValueURL(); }
        bool                        isValueURL() const { return !m_nodes.empty(); }
        bool                        isAbsoluteValueURL() const { return !isRelativeValueURL(); }
        bool                        isRelativeValueURL() const { return m_fileName.empty(); }

        const std::string&          getFileName() const { return m_fileName; }
        SerialURL                   toFileURL() const { return SerialURL(m_fileName); }
        const std::vector<std::string>& getNodes() const { return m_nodes; }
        SerialURL                   toRelativeValueURL() const;

        const std::string&          getNormalizedString() const { return m_normalized; 
        }
    private:
        /// ##### Internals
        void                        computeNormalizedString();

    private:
        /// ##### Attributes
        std::string                 m_fileName;
        std::vector<std::string>    m_nodes;
        std::string                 m_normalized;
    };

    bool operator<(const SerialURL& _left, const SerialURL& _right);
    bool operator==(const SerialURL& _left, const SerialURL& _right);
}

namespace std
{
    template<> struct hash<hjarta::SerialURL>
    {
        size_t operator()(hjarta::SerialURL const& _url) const noexcept
        {
            return std::hash<std::string>{}(_url.getNormalizedString());
        }
    };
}

#endif // HJARTA_SerialURL_h