#include "hjarta/serialization/SerialHook.h"

namespace hjarta
{
    SerialHooker::~SerialHooker()
    {
        for (ISerialHook* hook : m_hooks)
        {
            delete hook;
        }
    }

    //------------------------------------------------------------------------------------------

    void SerialHooker::removeHook(ISerialHook* _hook)
    {
        auto it = m_hooks.find(_hook);
        if (it != m_hooks.end())
        {
            m_hooks.erase(it);
            delete _hook;
        }
    }
}