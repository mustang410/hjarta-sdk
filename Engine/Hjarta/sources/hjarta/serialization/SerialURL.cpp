#include "hjarta/serialization/SerialURL.h"

#include "hjarta/tools/Log.h"
#include "hjarta/core/StringFacility.h"

namespace hjarta
{
    const char SerialURL::NodeSeparator = '/';
    const char SerialURL::FileDelimiter = ':';

    //------------------------------------------------------------------------------------------

    void SerialURL::serialize(Serializer& _serializer)
    {
        _serializer.serializeSimple(&m_normalized);
        if (_serializer.isUnserializing())
        {
            set(m_normalized);
        }
    }

    //------------------------------------------------------------------------------------------

    SerialURL::SerialURL(const std::string& _url)
    {
        set(_url);
    }

    //------------------------------------------------------------------------------------------
    
    void SerialURL::set(const std::string& _url)
    {
        size_t index = _url.find(FileDelimiter);
        if (index == std::string::npos)
        {
            // simple file
            m_fileName = _url;
        }
        else
        {
            // file
            m_fileName = _url.substr(0, index);
            m_nodes = StringFacility::split(_url.substr(index + 1), NodeSeparator);
        }

        computeNormalizedString();
    }

    //------------------------------------------------------------------------------------------

    void SerialURL::clear()
    {
        m_fileName = std::string{};
        m_nodes.clear();
        m_normalized = std::string{};
    }

    //------------------------------------------------------------------------------------------

    SerialURL SerialURL::appendNode(const std::string& _node) const
    {
        SerialURL url(*this);
        url.m_nodes.emplace_back(_node);
        url.computeNormalizedString();
        return url;
    }

    //------------------------------------------------------------------------------------------

    SerialURL SerialURL::appendRelativeURL(const SerialURL& _relativeURL) const
    {
        if (!HJARTA_VERIFY(_relativeURL.isRelativeValueURL(), "Cannot add non-relative URL <", _relativeURL.getNormalizedString(), "> to URL <", getNormalizedString(), ">") ||
            !HJARTA_VERIFY(isFileURL(), "Cannot add relative URL <", _relativeURL.getNormalizedString(), "> to non-file URL <", getNormalizedString(), ">"))
        {
            return SerialURL{};
        }

        SerialURL url(*this);
        url.m_nodes = _relativeURL.m_nodes;
        url.computeNormalizedString();
        return _relativeURL;
    }

    //------------------------------------------------------------------------------------------

    SerialURL SerialURL::toRelativeValueURL() const
    {
        SerialURL url;
        url.m_nodes = m_nodes;
        url.computeNormalizedString();
        return url;
    }

    //------------------------------------------------------------------------------------------

    void SerialURL::computeNormalizedString()
    {
        std::string normalized = m_fileName;
        if (!m_nodes.empty())
        {
            normalized += FileDelimiter;
            size_t index = 0;
            for (const std::string& node : m_nodes)
            {
                normalized += node;
                index++;
                if (index < m_nodes.size())
                    normalized += NodeSeparator;
            }
        }
        m_normalized = normalized;
    }

    //------------------------------------------------------------------------------------------

    bool operator<(const SerialURL& _left, const SerialURL& _right)
    {
        return _left.getNormalizedString() < _right.getNormalizedString();
    }

    //------------------------------------------------------------------------------------------

    bool operator==(const SerialURL& _left, const SerialURL& _right)
    {
        return _left.getNormalizedString() == _right.getNormalizedString();
    }
}