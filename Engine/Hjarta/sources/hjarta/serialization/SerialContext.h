#ifndef HJARTA_SerialContext_h
#define HJARTA_SerialContext_h

#include "hjarta/serialization/SerialURL.h"
#include "hjarta/serialization/jsoncpp/json.h"

namespace hjarta
{
    // ## SerialContext
    // Serializer context
    // Handle the current json pointer (relative to its root) and the SerialURL associated
    struct SerialContext
    {
        SerialContext() = default;
        SerialContext(Json::Value* _json, const SerialURL& _url) : m_currentJson(_json), m_currentURL(_url) {}
        Json::Value*            m_currentJson = nullptr;
        SerialURL               m_currentURL;
    };

    // ## SerialScopeContext
    // Perform a context copy and restore it at destruction
    class SerialScopeContext
    {
    public:
        SerialScopeContext(SerialContext& _context) : m_context(&_context), m_value(_context) {}
        ~SerialScopeContext() { *m_context = m_value; }

    private:
        /// ##### Attributes
        SerialContext*          m_context;
        SerialContext           m_value;
    };
}

#endif // HJARTA_SerialContext_h