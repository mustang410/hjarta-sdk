#ifndef HJARTA_ISerializable_h
#define HJARTA_ISerializable_h

namespace hjarta
{
    class Serializer;
    class SerialURL;

    // ## ISerializable
    // Base class for simple serializable classes
    class ISerializable
    {
    public:
        /// ##### Serialization
        virtual void serialize(Serializer& _serializer) = 0;
        virtual void setSerialURL(const SerialURL& _url) {};
    };
}

#endif // HJARTA_ISerializable_h