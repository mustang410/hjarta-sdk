#ifndef HJARTA_Archive_h
#define HJARTA_Archive_h

#include "hjarta/serialization/SerialURL.h"
#include "hjarta/serialization/jsoncpp/json.h"

#include <string>

namespace hjarta
{
    // ## Archive
    // Handle the manipulation of json data files
    class Archive
    {
    public:
        // ### Status
        // An archive can be:
        // Loaded (from a file, without error)
        // Unsaved (set, waiting to be saved)
        // Saved
        // Faulty (from a failed loading attempt)
        enum class Status { Unset, Loaded, Saved, Faulty };

    public:
        /// ##### Constructors & destructors
        // Create a default archive in unsaved mode
        Archive() = default;
        // Try to create an archive by loading content
        Archive(const SerialURL &_url);
        // Copy & destructor
        Archive(const Archive &_copy);
        ~Archive() = default;

        const SerialURL&    getURL() const { return m_url; }

        /// ##### Archive status
        bool                isLoaded() const { return m_status == Status::Loaded; }
        bool                isSaved() const { return m_status == Status::Saved; }
        bool                isFaulty() const { return m_status == Status::Faulty; }

        /// ##### Archive interface
        bool                load(const SerialURL& _url);
        bool                save(const SerialURL& _url);
        Json::Value&        getJsonValue() { return m_value; }

    private:
        /// ##### Attributes
        Status              m_status = Status::Unset;
        SerialURL           m_url;
        Json::Value         m_value;

    };
}

#endif // HJARTA_Archive_h