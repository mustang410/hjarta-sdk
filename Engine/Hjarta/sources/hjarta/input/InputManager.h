#ifndef HJARTA_InputManager_h
#define HJARTA_InputManager_h

#include "hjarta/core/Types.h"
#include "hjarta/event/EventDispatcher.h"

#define HJARTA_INPUT_MANAGER() hjarta::InstanceProvider<hjarta::InputManager>::get()

namespace hjarta
{
    class InputManager : public EventDispatcher
    {
    public:
        /// ##### Custom types
        enum class TextInputType { All, AlphaNumeric, OnlyAlpha, OnlyNumeric };
        enum class Mode { Free, TextInput };

    public:
        /// ##### InputManager interface
        void            update();

        const Point2D<int>& getMousePosition() const { return m_mousePosition; }
        const bool&     hasQuitRequest() const { return m_quitRequest; }

        /// ##### Text mode
        void            initTextInput(const std::wstring& _init, const TextInputType& _textInputType = TextInputType::All);
        void            stopTextInput();
        void            setTextInput(const std::wstring& _text) { m_textInput = _text; }

    private:
        /// ##### Attributes
        Mode            m_mode = Mode::Free;

        // mouse
        Point2D<int>    m_mousePosition;
        bool            m_quitRequest = false;
        // text input mode
        TextInputType   m_textInputType;
        std::wstring    m_textInput;
    };
}

#endif // HJARTA_InputManager_h