#ifndef HJARTA_InputEvent_h
#define HJARTA_InputEvent_h

#include "hjarta/event/Event.h"
#include "hjarta/core/Types.h"

namespace hjarta
{
    class InputEvent : public Event
    {
        HJARTA_DECL_EVENT(hjarta::InputEvent, hjarta::Event)
    };

    //------------------------------------------------------------------------------------------

    class InputEventKey : public InputEvent
    {
        HJARTA_DECL_EVENT(hjarta::InputEventKey, hjarta::InputEvent)

    public:
        /// ##### Custom types
        enum Type { KeyUp, KeyDown };

    public:
        InputEventKey() = default;
        InputEventKey(Type _type, ScanCode _scancode) : m_type(_type), m_scancode(_scancode) {}
        Type        getType() const { return m_type; }
        ScanCode    getScancode() const { return m_scancode; }

    private:
        /// ##### Attributes
        Type        m_type;
        ScanCode    m_scancode;
    };

    //------------------------------------------------------------------------------------------

    class InputEventMouseMotion : public InputEvent
    {
        HJARTA_DECL_EVENT(hjarta::InputEventMouseMotion, hjarta::InputEvent)

    public:
        InputEventMouseMotion() = default;
        InputEventMouseMotion(const Point2D<int>& _position) : m_position(_position) {}
        const Point2D<int>& getPosition() const { return m_position; }

    private:
        /// ##### Attributes
        Point2D<int> m_position;
    };

    //------------------------------------------------------------------------------------------

    class InputEventMouseButton : public InputEventMouseMotion
    {
        HJARTA_DECL_EVENT(hjarta::InputEventMouseButton, hjarta::InputEventMouseMotion)

    public:
        /// ##### Custom types
        enum Type { ButtonUp, ButtonDown, WheelScroll };

    public:
        InputEventMouseButton() = default;
        InputEventMouseButton(Type _type, Uint8 _button, const Point2D<int>& _position) : InputEventMouseMotion(_position),
            m_type(_type), m_button(_button) {}
        InputEventMouseButton(int _scroll, const Point2D<int>& _position) : InputEventMouseMotion(_position),
            m_type(WheelScroll), m_scroll(_scroll) {}
        Type        getType() const { return m_type; }
        Button      getButton() const { return m_button; }
        int         getScroll() const { return m_scroll; }

    private:
        /// ##### Attributes
        Type        m_type;
        Button      m_button;
        int         m_scroll = 0;
    };

    //------------------------------------------------------------------------------------------

    class InputEventText : public InputEvent
    {
        HJARTA_DECL_EVENT(hjarta::InputEventText, hjarta::InputEvent)

    public:
        InputEventText() = default;
        InputEventText(const std::wstring &_text) : m_text(_text) {}
        const std::wstring &getText() const { return m_text; }

    private:
        /// ##### Attributes
        std::wstring m_text;
    };
}

#endif // HJARTA_InputEvent_h