#include "hjarta/input/InputManager.h"
#include "hjarta/input/InputEvent.h"

#include <string>
#include <locale>
#include <codecvt>
#include <sstream>

namespace hjarta
{
    void InputManager::update()
    {
        // converter used to retrieve UTF8 text inputs
        static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

        SDL_Event evt;
        while (SDL_PollEvent(&evt))
        {
            switch (evt.type)
            {
            case SDL_KEYUP:
                if (m_mode == Mode::Free)
                {
                    sendEvent<InputEventKey>(InputEventKey::KeyUp, evt.key.keysym.scancode);
                }
                break;

            case SDL_KEYDOWN:
                if (m_mode == Mode::Free)
                {
                    sendEvent<InputEventKey>(InputEventKey::KeyDown, evt.key.keysym.scancode);
                }
                else if (m_mode == Mode::TextInput) // special character handling for text input mode
                {
                    if (evt.key.keysym.scancode == SDL_SCANCODE_BACKSPACE) // backspace
                    {
                        if (m_textInput.length() > 0)
                        {
                            m_textInput.erase(m_textInput.end() - 1); // remove last character
                            sendEvent<InputEventText>(m_textInput);
                        }
                    }
                }
                break;

            case SDL_MOUSEBUTTONUP:
                sendEvent<InputEventMouseButton>(InputEventMouseButton::ButtonUp, evt.button.button, m_mousePosition);
                break;

            case SDL_MOUSEBUTTONDOWN:
                sendEvent<InputEventMouseButton>(InputEventMouseButton::ButtonDown, evt.button.button, m_mousePosition);
                break;

            case SDL_MOUSEMOTION:
                m_mousePosition.x = evt.motion.x;
                m_mousePosition.y = evt.motion.y;
                sendEvent<InputEventMouseMotion>(m_mousePosition);
                break;

            case SDL_MOUSEWHEEL:
                sendEvent<InputEventMouseButton>(evt.wheel.y, m_mousePosition);
                break;

            case SDL_QUIT:
                m_quitRequest = true;
                break;

            case SDL_TEXTINPUT:
                if (m_mode == Mode::TextInput)
                {
                    if (m_textInputType == TextInputType::All)
                    {
                        m_textInput += converter.from_bytes(evt.text.text);
                    }
                    else
                    {
                        std::wstring buffer;
                        for (unsigned i = 0; i < 32; i++)
                        {
                            wchar_t c = evt.text.text[i];
                            if (c == '\0') break;

                            if ((c >= 'a'  && c <= 'z') || (c >= 'A'  && c <= 'Z'))
                            {
                                if (m_textInputType == TextInputType::AlphaNumeric || m_textInputType == TextInputType::OnlyAlpha)
                                {
                                    buffer += c;
                                }
                            }
                            else if (c >= '0' && c <= '9')
                            {
                                if (m_textInputType == TextInputType::AlphaNumeric || m_textInputType == TextInputType::OnlyNumeric)
                                {
                                    buffer += c;
                                }
                            }
                        }
                        m_textInput += buffer;
                        sendEvent<InputEventText>(m_textInput);
                    }
                }
                break;

            default:
                break;
            }
        }
    }
    
    //------------------------------------------------------------------------------------------

    void InputManager::initTextInput(const std::wstring &_init, const TextInputType &_textInputType)
    {
        if (m_mode == Mode::Free) SDL_StartTextInput();
        m_textInputType = _textInputType;
        m_mode = Mode::TextInput;
        m_textInput = _init;
    }

    //------------------------------------------------------------------------------------------

    void InputManager::stopTextInput()
    {
        if (m_mode == Mode::TextInput) SDL_StopTextInput();
        m_mode = Mode::Free;
        m_textInput = std::wstring(); // reset handled text
    }
}